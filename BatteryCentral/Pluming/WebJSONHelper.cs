﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Net;
using System.IO;

namespace Pluming
{
    public class WebJSONHelper : TaskHandler
    {

        public WebJSONHelper(object o)
        {
            WorkItemQueries = (List<string>)o;
        }
        private List<string> WorkItemQueries = null;


        String DoQuery(string query)
        {
            try
            {

                WebRequest wr = HttpWebRequest.Create(query);

                wr.Timeout = 300000;

                using (WebResponse response = wr.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            catch (Exception )
            {
                return null;
            }
        }

        public FullQuery DecodeAndGenerateDataQueryObjectFromStringDownload(String json_string)
        {
            dynamic d = JSON.JsonDecode(json_string);
            foreach (dynamic m in d["response"])
            {
                string cif = m["cif"];
                cif = cif.StringToHEXAscii();
                m["cif"] = cif;

                if (m["icsd_id"] is System.Collections.ArrayList)
                {
                    if (m["icsd_id"].Count == 0)
                    {
                        m["icsd_id"] = 0;
                    }
                }
                if (m["icsd_id"] == null)
                {
                    m["icsd_id"] = 0;
                }
            }
            string ms = JSON.JsonEncode(d);
            FullQuery fq = BsonSerializer.Deserialize<FullQuery>(ms);

            return fq;

        }
        ElementalCRUD CRUD = new ElementalCRUD("Chaos");


        public override void TaskHandlerThreadProc()
        {

            foreach (string s in WorkItemQueries)
            {
                String JSON_QUERY_RESULT = DoQuery(s);    // this calls out over the internet, and pulls the json query.

                string empty_string = "" + (char)(0x00);
                JSON_QUERY_RESULT = JSON_QUERY_RESULT.Replace(empty_string, "");

                FullQuery fq = DecodeAndGenerateDataQueryObjectFromStringDownload(JSON_QUERY_RESULT);


                foreach (MSGPMolecule molecule in fq.Molecules)
                {
                    CRUD.AddMolecule(molecule);
                    Post("Processed Molecule: " + molecule.MaterialID);
                }
            }
            Post("Done With All Queries.");
        }

        public static object[] GenerateWorkSet(int thread_count)
        {
            RequestBuilder rb = new RequestBuilder();
            string[] items = File.ReadAllLines("DatabaseIDs.ids");

            object[] objects = new object[thread_count];
            int i = 0;
            for (i = 0; i < thread_count; i++)
            {
                objects[i] = new List<string>();
            }
            i = 0;
            foreach (string s in items)
            {
                ((List<string>)objects[i]).Add(rb.BuildRequest(s));
                i = (i + 1) % thread_count;
            }
            return objects;
        }
    }

    public class RequestBuilder
    {
        public string BuildRequest(string molecule_id)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Preamble);
            sb.Append(molecule_id);
            sb.Append(Builder);
            sb.Append(MSDBKey);
            return sb.ToString();
        }


        private const string Builder = "/vasp";
        private const string Preamble = "https://www.materialsproject.org/rest/v1/materials/";

        private const string MSDBKey = "?API_KEY=SfU2umT8DitQBLP6";

    }


}