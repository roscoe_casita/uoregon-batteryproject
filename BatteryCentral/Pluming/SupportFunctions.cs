﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Numerics;

namespace Pluming
{
    public static class Extensions
    {
        public static String StringToHEXAscii(this string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in s)
            {
                sb.Append(((byte)c).ToString("X2"));

            }
            return sb.ToString();
        }

        public static String HEXAsciiToString(this string s)
        {
            s = s.Replace(" ", "");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i + 1 < s.Length; i += 2)
            {
                sb.Append((char)byte.Parse(s.Substring(i, 2), NumberStyles.HexNumber));
            }
            return sb.ToString();
        }       
        
        public static string ToFullExceptonString(this Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            while (ex != null)
            {
                sb.AppendLine(ex.Message);
                ex = ex.InnerException;
            }
            return sb.ToString();
        }

        public static int BitCount(this BigInteger bi)
        {
            int count = 0;
            BigInteger c = 1;

            while (c <= bi)
            {
                if ((c | bi) == bi)
                {
                    count++;
                }
                c = c << 1;
            }
            return count;
        }

        public static string ConvertToDisplayString(this KeyValuePair<BigInteger, Queue<BigInteger>> bis)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(bis.Key.ConvertToBinaryString() + " : " + bis.Value.Count() + " -> ");

            foreach (BigInteger bi in bis.Value)
            {
                sb.Append(bi.ConvertToBinaryString());
                sb.Append(",");
            }
            return sb.ToString();
        }
        public static string ConvertToBinaryString(this BigInteger bi)
        {
            StringBuilder sb = new StringBuilder();

            if (bi.CompareTo(0) == 0)
            {
                return "0";
            }
            BigInteger i = 1;
            while (i <= bi)
            {
                i = i << 1;
            }
            i = i >> 1;
            while (i > 0)
            {
                if ((i & bi) == i)
                {
                    sb.Append("1");
                }
                else
                {
                    sb.Append("0");
                }
                i = i >> 1;
            }
            return sb.ToString();
     
        
        
        }
    }

    public class PowerSetByN
    {


        public IEnumerable<KeyValuePair<BigInteger, Queue<BigInteger>>> Enumerate(int TwoToN)
        {
            Queue<BigInteger> StackQueue = new Queue<BigInteger>(TwoToN * 2);

            yield return new KeyValuePair<BigInteger, Queue<BigInteger>>(0, StackQueue);
            foreach (KeyValuePair<BigInteger, bool> kvp in GenList(0, TwoToN))
            {
                yield return new KeyValuePair<BigInteger, Queue<BigInteger>>(kvp.Key, StackQueue);
                if (kvp.Value)
                {
                    StackQueue.Enqueue(kvp.Key);
                }
            }
            while (StackQueue.Count > 0)
            {
                foreach (KeyValuePair<BigInteger, bool> kvp in GenList(StackQueue.Dequeue(), TwoToN))
                {
                    yield return new KeyValuePair<BigInteger, Queue<BigInteger>>(kvp.Key, StackQueue);
                    if (kvp.Value)
                    {
                        StackQueue.Enqueue(kvp.Key);
                    }
                }
            }
        }

        private IEnumerable<KeyValuePair<BigInteger, bool>> GenList(BigInteger permute, int TwoToN)
        {

            BigInteger max = BigInteger.Pow(2, TwoToN);

            BigInteger start = max >> 1;

            while ((start | permute) != permute)
            {


                bool test = (start | permute).BitCount() < 4;
                if ((start & max) == max)
                {
                    test = false;
                }


                yield return new KeyValuePair<BigInteger, bool>(start | permute, test);
                start = start >> 1;
            }
        }

    }


    public class TupleList<T1, T2> : List<Tuple<T1, T2>> where T1 : IComparable
    {
        public void Add(T1 item, T2 item2)
        {
            Add(new Tuple<T1, T2>(item, item2));
        }

        public new void Sort()
        {
            Comparison<Tuple<T1, T2>> c = (a, b) => a.Item1.CompareTo(b.Item1);
            base.Sort(c);
        }

    }


}
