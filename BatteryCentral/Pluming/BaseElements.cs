﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluming
{
    public enum Element
    {
        H,
        He,
        Li,
        Be,
        B,
        C,
        N,
        O,
        F,
        Ne,
        Na,
        Mg,
        Al,
        Si,
        P,
        S,
        Cl,
        Ar,
        K,
        Ca,
        Sc,
        Ti,
        V,
        Cr,
        Mn,
        Fe,
        Co,
        Ni,
        Cu,
        Zn,
        Ga,
        Ge,
        As,
        Se,
        Br,
        Kr,
        Rb,
        Sr,
        Y,
        Zr,
        Nb,
        Mo,
        Tc,
        Ru,
        Rh,
        Pd,
        Ag,
        Cd,
        In,
        Sn,
        Sb,
        Te,
        I,
        Xe,
        Cs,
        Ba,
        La,
        Ce,
        Pr,
        Nd,
        Pm,
        Sm,
        Eu,
        Gd,
        Tb,
        Dy,
        Ho,
        Er,
        Tm,
        Yb,
        Lu,
        Hf,
        Ta,
        W,
        Re,
        Os,
        Ir,
        Pt,
        Au,
        Hg,
        Tl,
        Pb,
        Bi,
        At,
        Rn,
        Fr,
        Ra,
        Ac,
        Th,
        Pa,
        U,
        Np,
        Pu,
        Am,
        Cm,
        Bk,
        Cf,
        Es,
        Fm,
        Md,
        No,
        Lr,
        Rf,
        Db,
        Sg,
        Bh,
        Hs,
        Mt,
        Ds,
        Rg,
        Cn,
        Uut,
        Fl,
        Uup,
        Lv,
        Uus
    }

    public static class ElementHelper
    {

        public static IEnumerable<Element> AllElements
        {
            get
            {
                foreach (var value in Enum.GetValues(typeof(Element)))
                {
                    yield return (Element)value;
                }
            }
        }

        public static IEnumerable<List<Element>> AllElementsLists
        {
            get
            {
                foreach (Element e in AllElements)
                {
                    yield return FromOne(e);
                }
            }
        }

        private static List<Element> FromOne(Element e)
        {
            List<Element> returnValue = new List<Element>();
            returnValue.Add(e);
            return returnValue;
        }

        public static IEnumerable<Element> StringsToElements(IEnumerable<string> element_names)
        {
            List<Element> elements = new List<Element>();

            foreach (string s in element_names)
            { 
                elements.Add((Element)Enum.Parse(typeof(Element), s));

            }
            elements.Sort();
            return elements;
        }


        public static IEnumerable<Element> StringToElements(string s)
        {
            string [] splitter = {"-"};
            List<Element> elements = new List<Element>();
            foreach (string e in s.Split(splitter, StringSplitOptions.RemoveEmptyEntries))
            {
                elements.Add((Element)Enum.Parse(typeof(Element), e));
            }
            elements.Sort();
            return elements;
        }
        public static string ElementsToString(IEnumerable<Element> elements)
        {
            StringBuilder sb = new StringBuilder();
            List<Element> els = elements.ToList();
            els.Sort();

            foreach (Element e in els)
            {
                sb.Append(e.ToString());
                sb.Append("-");
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        public static Element StringToElement(string s)
        {
            return (Element)Enum.Parse(typeof(Element), s);
        }

        public static int CompareCompounds(CompactMolecule X, CompactMolecule Y)
        {
            int returnValue =  X.Elements.Length.CompareTo(Y.Elements.Length);

            if(returnValue == 0)
            {
                for (int i = 0; i < X.Elements.Length; i++)
                {
                    returnValue = X.Elements[i].CompareTo(Y.Elements[i]);

                    if (returnValue != 0)
                    {
                        return returnValue;
                    }
                }

                foreach (KeyValuePair<Element, int> kvp in X.UnitCellFormula)
                {
                    returnValue = kvp.Value.CompareTo(Y.UnitCellFormula[kvp.Key]);

                    if (returnValue != 0)
                    {
                        return returnValue;
                    }
                }
                return returnValue;
            }

            return returnValue;

        }
    }
}
