﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SevenZip;

namespace Pluming
{

    public class CompressedFileSystem
    {
        public CompressedFileSystem(string directory_path)
        {
            sevenZDllPath = Path.GetFullPath("7z.dll");
            SevenZip.SevenZipExtractor.SetLibraryPath(sevenZDllPath);
            SevenZip.SevenZipCompressor.SetLibraryPath(sevenZDllPath);

            DirectoryPath = directory_path;
            //CompressDirectoryFiles(DirectoryPath);

        }

        public string GetFileName(string FileName)
        {
            return DirectoryPath + FileName + ".7z";
        }

        string sevenZDllPath;

        string DirectoryPath;

        public bool Exists(string Filename)
        {
            return File.Exists(DirectoryPath + Filename + ".7z");
        }
        public IEnumerable<string> FileNames
        {
            get
            {
                foreach (String s in Directory.EnumerateFiles(DirectoryPath))
                {
                    yield return CompressedFileSystem.PathToFileName(s.Replace(".7z", ""));
                }
            }
        }

        public void SetFile(string Filename, MemoryStream stream)
        {
            stream.Position = 0;
            SetFile(Filename, stream.ToArray());
        }

        public void SetFile(string Filename, string contents)
        {
            byte[] bytes = new byte[contents.Length * sizeof(char)];
            System.Buffer.BlockCopy(contents.ToCharArray(), 0, bytes, 0, bytes.Length);

            SetFile(Filename, bytes);
        }

        public void DeleteFile(string Filename)
        {
            string compressedFilePath = DirectoryPath + Filename + ".7z";

            File.Delete(compressedFilePath);
        }

        public void SetFile(string Filename, byte [] array)
        {
            File.WriteAllBytes(DirectoryPath + Filename, array);

            SevenZip.SevenZipCompressor szc = new SevenZip.SevenZipCompressor();
            szc.CompressionLevel = SevenZip.CompressionLevel.Ultra;
            szc.CompressionMode = SevenZip.CompressionMode.Create;

            string compressedFilePath = DirectoryPath + Filename + ".7z";

            szc.CompressionMode = SevenZip.CompressionMode.Create;
            FileStream archive = new FileStream(compressedFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            szc.DirectoryStructure = true;
            szc.EncryptHeaders = true;


            szc.DefaultItemName = Filename; //if the full path given the folders are also created

            szc.CompressFiles(archive, DirectoryPath + Filename);
            archive.Close();

            File.Delete(DirectoryPath + Filename);
        }

        public MemoryStream GetFile(string Filename)
        {

            SevenZip.SevenZipExtractor sze = new SevenZipExtractor(DirectoryPath + Filename + ".7z");

            MemoryStream ms = new MemoryStream();
            sze.ExtractFile(Filename, ms);
            
            sze.Dispose();
            ms.Position = 0;

            return ms;
        }


        public static void CompressDirectoryFiles(string DirectoryPath)
        {

            SevenZip.SevenZipCompressor szc = new SevenZip.SevenZipCompressor();
            szc.CompressionLevel = SevenZip.CompressionLevel.Ultra;
            szc.CompressionMode = SevenZip.CompressionMode.Create;
            foreach (string filePath in Directory.EnumerateFiles(DirectoryPath))
            {
                if (!filePath.EndsWith(".7z"))
                {
                    string compressedFilePath = filePath + ".7z";
                    szc.CompressionMode = File.Exists(compressedFilePath) ? SevenZip.CompressionMode.Append : SevenZip.CompressionMode.Create;
                    FileStream archive = new FileStream(compressedFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    szc.DirectoryStructure = true;
                    szc.EncryptHeaders = true;

                    string fileName = PathToFileName(filePath);
                    szc.DefaultItemName = fileName; //if the full path given the folders are also created
                    szc.CompressFiles(archive, filePath);
                    archive.Close();

                    File.Delete(filePath);
                }
            }
        }

        private static string PathToFileName(string s)
        {
            int index = s.LastIndexOf("\\") + 1;
            return s.Substring(index, s.Length - index);
        }

        public static void UnCompressDirectoryFiles(string DirectoryPath)
        {

            foreach (string s in Directory.EnumerateFiles(DirectoryPath))
            {
                if (!s.EndsWith(".7z"))
                {
                    continue;
                }

                string uncompressed = s.Replace(".7z", "");
                SevenZip.SevenZipExtractor sze = new SevenZipExtractor(s);

                FileStream fs = new FileStream(uncompressed, FileMode.Create);
                sze.ExtractFile(PathToFileName(uncompressed), fs);
                fs.Close();

                sze.Dispose();
                File.Delete(s);
            }
        }
    }
}
