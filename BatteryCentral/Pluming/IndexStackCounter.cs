﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluming
{

    public class IndexStackCounter<T>
    {
        public IndexStackCounter(IEnumerable<T> index)
        {
            Index = index.ToArray();
            CurrentIndex = new List<int>();
            Reset();
        }

        public void Reset()
        {
            CurrentIndex.Clear();
            CurrentIndex.Add(Index.Length - 1);
        }

        public bool Advance()
        {
            if (CurrentIndex.Count > 0)
            {
                int val = Last - 1;
                if (val < 0)
                {
                    return false;
                }
                else
                {
                    CurrentIndex.RemoveAt(CurrentIndex.Count - 1);
                    CurrentIndex.Add(val);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }


        public bool Unwind()
        {
            if (CurrentIndex.Count == 0)
            {
                return false;
            }
            else
            {
                CurrentIndex.RemoveAt(CurrentIndex.Count - 1);
                return true;
            }
        }

        public bool Dive()
        {
            int val = Last - 1;
            if (val < 0)
            {
                return false;
            }
            else
            {
                CurrentIndex.Add(val);
                return true;
            }
        }

        private int Last
        {
            get
            {
                return CurrentIndex[CurrentIndex.Count - 1];
            }
        }

        public IEnumerable<T> EnumerateCurrentIndex
        {
            get
            {
                foreach (int i in CurrentIndex)
                {
                    yield return Index[i];
                }
            }
        }
        public List<int> GetCurrentIndex
        {
            get
            {
                return CurrentIndex;
            }
        }
        T[] Index = null;
        List<int> CurrentIndex = null;
    }
}
