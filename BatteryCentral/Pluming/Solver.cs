﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluming
{
    public class Solver
    {
        // finally found the solution in generalized Row-Reduced Eschellon form here over fields:
        //https://jeremykun.com/2011/12/30/row-reduction-over-a-field/

        public static Rational[,] rref(Rational[,] M)
        {
            Rational zero = new Rational(0);
            int numRows = M.GetLength(0);
            int numCols = M.GetLength(1);

            int i = 0;
            int j = 0;

            while (true)
            {
                if (i >= numRows || j >= numCols)
                {
                    break;
                }
                int nonZeroRow = i;
                while (nonZeroRow < numRows && (M[nonZeroRow, j].CompareTo(zero) == 0))
                {
                    nonZeroRow++;
                }
                if (nonZeroRow >= numRows)
                {
                    j += 1;
                    continue;
                }
                if (i != nonZeroRow)
                {
                    SwapRows(M, nonZeroRow, i);
                }
                Rational pivot = M[i, j];

                // Matrix should now be 1 in the ith of jth position.
                MultiplyRow(M, i, pivot.Inverse);

                for (int otherRow = 0; otherRow < numRows; otherRow++)
                {
                    if (i == otherRow)
                    {
                        continue;
                    }
                    if (M[otherRow, j].CompareTo(zero) != 0)
                    {
                        Rational factor = new Rational(-1) * M[otherRow, j];

                        AddMultiplesOfRow(M, i, otherRow, factor);
                    }
                }
                i++;
                j++;
            }
            return M;
        }

        private static void AddMultiplesOfRow(Rational[,] matrix, int row1, int row2, Rational factor)
        {
            int numRows = matrix.GetLength(0);
            int numCols = matrix.GetLength(1);

            for (int c = 0; c < numCols; c++)
            {
                matrix[row2, c] += matrix[row1, c] * factor;
            }
        }

        private static void MultiplyRow(Rational[,] matrix, int row, Rational factor)
        {
            int numRows = matrix.GetLength(0);
            int numCols = matrix.GetLength(1);

            for (int c = 0; c < numCols; c++)
            {
                matrix[row, c] = matrix[row, c] * factor;
            }
        }
        private static void SwapRows(Rational[,] matrix, int row1, int row2)
        {
            int numRows = matrix.GetLength(0);
            int numCols = matrix.GetLength(1);

            Rational tmp = null;
            for (int j = 0; j < numCols; j++)
            {
                tmp = matrix[row1, j];
                matrix[row1, j] = matrix[row2, j];
                matrix[row2, j] = tmp;
            }
        }
    }
}
