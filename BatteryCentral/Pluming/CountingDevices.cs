﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Numerics;

namespace Pluming
{
    namespace CountingDevices
    {


        public static class Extensions
        {

            public static int MinSizeN(this BigInteger s)
            {
                int count = 0;
                BigInteger i = 1;
                while (i < s)
                {
                    i = i << 1;
                    count++;
                }
                return count;
            }

            public static string ToBinString(this BigInteger s)
            {
                return ToBinString(s, 0);
            }

            public static string ToBinString(this BigInteger s, BigInteger maxnum)
            {
                if (s < 0)
                {
                    throw new Exception("Negative numbers not supported.");
                }

                StringBuilder sb = new StringBuilder();
                BigInteger val = 1;


                while (val <= s)
                {
                    if ((val & s) == val)
                    {
                        sb.Insert(0, "1");
                    }
                    else
                    {
                        sb.Insert(0, "0");
                    }
                    val = val << 1;
                }

                while (val <= maxnum)
                {
                    sb.Insert(0, "0");
                    val = val << 1;
                }
                return sb.ToString();
            }

            public static BigInteger BinStringToBigInt(this string bi)
            {
                BigInteger returnValue = new BigInteger();

                foreach (char c in bi)
                {
                    returnValue = returnValue << 1;
                    switch (c)
                    {
                        case '0':
                            break;
                        case '1':
                            returnValue = returnValue | 1;
                            break;
                        default:
                            throw new Exception("WRONG STRING.");
                    }
                }

                return returnValue;
            }

            public static IEnumerable<BigInteger> StackRange(this IEnumerable<Counter> counters)
            {
                foreach (Counter c in counters)
                {
                    StackCounter sc = new StackCounter(c);
                    foreach (BigInteger bi in sc.EnumerateRange())
                    {
                        yield return bi;
                    }
                }
            }
            /// <summary>
            /// Transform a Big Integer, into the equivelent stack counters, so we can advance them. 
            /// </summary>
            /// <param name="number">The Big Integer to start at.</param>
            /// <returns>the new stack/list of indexs of the 1's</returns>
            /// 
            public static List<int> GenStackFromNumber(this BigInteger number)
            {
                BigInteger max = 1;
                List<int> returnValue = new List<int>();
                int count = 0;
                while (max <= number)
                {
                    if ((max & number) == max)
                    {
                        returnValue.Add(count);
                    }
                    count++;
                    max = max << 1;
                }
                return returnValue;
            }
            /// <summary>
            /// Generate the Big Integer from the stack of counters
            /// </summary>
            /// <param name="sc">stack of counters.</param>
            /// <returns>element N of the count</returns>
            public static BigInteger GenNumber(this IEnumerable<int> sc)
            {
                BigInteger returnValue = 0;
                BigInteger test = 0;
                foreach (int i in sc)
                {
                    test = returnValue | BigInteger.Pow(2, i);
                    if (test == returnValue) { throw new Exception("INVALID NUMBER GENERATION."); }
                    returnValue = test;
                }
                return returnValue;
            }
        }

        public class StackCounterEnumerator : IEnumerator<BigInteger>
        {
            public StackCounterEnumerator(BigInteger start, BigInteger stop)
            {
                counter = new Counter(start, stop);
            }
            public StackCounterEnumerator(Counter c)
            {
                counter = new Counter(c.Start, c.Stop);
            }

            Counter counter;
            List<int> CountingStack;

            public BigInteger Current
            {
                get
                {
                    return counter.Start;
                }
            }

            public void Dispose()
            {
                if (CountingStack != null)
                {
                    CountingStack.Clear();
                    CountingStack = null;
                }
            }

            object System.Collections.IEnumerator.Current
            {
                get { return counter.Start; }
            }

            public bool MoveNext()
            {
                if (CountingStack == null)
                {
                    CountingStack = counter.Start.GenStackFromNumber();
                    return true;
                }
                if (counter.Start == counter.Stop)
                {
                    return false;
                }
                else
                {
                    bool done = false;
                    int stackControl = 0;

                    do
                    {
                        if (CountingStack[stackControl] == stackControl)
                        {
                            stackControl++;
                            if (stackControl >= CountingStack.Count)
                            {
                                done = true;
                            }
                        }
                        else
                        {
                            int val = CountingStack[stackControl] - 1;

                            do
                            {
                                CountingStack[stackControl] = val--;
                            } while (stackControl-- > 0);

                        }
                    } while ((stackControl > 0) && (!done));

                    if (!done)
                    {
                        counter.Start = CountingStack.GenNumber();
                    }
                    else
                    {
                        counter.Start = counter.Stop;
                    }
                    return !done;
                }

            }

            public void Reset()
            {
                throw new NotImplementedException();
            }


            public StackCounterEnumerator GenNextDepth()
            {
                if (
                        (
                            ((counter.Start & 1) != 1) &&
                            (counter.Start != counter.Stop)
                        )
                    )
                {
                    BigInteger bi = 1;

                    while ((bi & counter.Start) != bi)
                    {
                        bi <<= 1;
                    }
                    bi >>= 1;

                    BigInteger start = counter.Start | bi;
                    BigInteger stop = counter.Start | 1;

                    return new StackCounterEnumerator(start, stop);
                }
                return null;
            }

        }

        public class StackCounter
        {
            public StackCounter(Counter init)
            {
                counter = init;
            }
            Counter counter;

            public BigInteger GenNextAndAdvance()
            {
                BigInteger returnValue = counter.Start;

                if (counter.Start != counter.Stop)
                {
                    List<int> CountingStack = counter.Start.GenStackFromNumber();

                    bool done = false;
                    int stackControl = 0;

                    do
                    {
                        if (CountingStack[stackControl] == stackControl)
                        {
                            stackControl++;
                            if (stackControl >= CountingStack.Count)
                            {
                                done = true;
                            }
                        }
                        else
                        {
                            int val = CountingStack[stackControl] - 1;

                            do
                            {
                                CountingStack[stackControl] = val--;
                            } while (stackControl-- > 0);

                        }
                    } while ((stackControl > 0) && (!done));

                    counter.Start = CountingStack.GenNumber();
                }

                return returnValue;
            }

            public IEnumerable<BigInteger> EnumerateRange()
            {

                List<int> CountingStack = counter.Start.GenStackFromNumber();
                bool done = false;
                int stackControl = -1;

                do
                {
                    if (stackControl < 0)
                    {
                        BigInteger returnValue = CountingStack.GenNumber();
                        yield return returnValue;

                        if (returnValue == counter.Stop)
                        {
                            done = true;
                        }
                        stackControl = 0;
                    }
                    else
                    {
                        do
                        {
                            if (CountingStack[stackControl] == stackControl)
                            {
                                stackControl++;
                                if (stackControl >= CountingStack.Count)
                                {
                                    done = true;
                                }
                            }
                            else
                            {
                                int val = CountingStack[stackControl] - 1;

                                do
                                {
                                    CountingStack[stackControl] = val--;
                                } while (stackControl-- > 0);

                            }
                        } while ((stackControl > 0) && (!done));
                    }
                } while (!done);
            }

        }

        public struct Counter
        {

            public Counter(BigInteger start, BigInteger stop)
            {
                Start = start;
                Stop = stop;
            }



            public Counter(string BinStringStart, string BinStringStop)
            {
                if (BinStringStart.Length != BinStringStop.Length)
                {
                    throw new Exception("Invalid Start & Stop String Lengths.");
                }
                //if (BinStringStart.Count(N => N.CompareTo('1') == 0) !=
                //    BinStringStop.Count(N => N.CompareTo('1') == 0))
                //{
                //    throw new Exception("Invalid Number of '1's in start/stop string.");
                //}
                //if (BinStringStart.Count(N => N.CompareTo('0') == 0) !=
                //    BinStringStop.Count(N => N.CompareTo('0') == 0))
                //{
                //    throw new Exception("Invalid Number of '0's in start/stop string.");
                //}

                Start = BinStringStart.BinStringToBigInt();
                Stop = BinStringStop.BinStringToBigInt();
            }
            public BigInteger Start;
            public BigInteger Stop;

        }

    }
}