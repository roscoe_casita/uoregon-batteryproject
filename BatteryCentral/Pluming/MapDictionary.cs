﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluming
{
    public class Map<A, B>
    {
        public Map() { }

        public int Count
        {
            get
            {
                return AToB.Count;
            }
        }

        public IEnumerable<A> EnumA
        {
            get
            {
                return AToB.Keys;
            }
        }

        public IEnumerable<B> EnumB
        {
            get
            {
                return BToA.Keys;
            }
        }

        public bool Contains(A a)
        {
            return AToB.ContainsKey(a);
        }

        public bool Contains(B b)
        {
            return BToA.ContainsKey(b);
        }
        public B this[A atob]
        {
            get
            {
                return AToB[atob];
            }
            set
            {
                if (!AToB.ContainsKey(atob))
                {
                    AToB.Add(atob, value);
                    BToA.Add(value, atob);
                }
                else
                {
                    AToB[atob] = value;
                    BToA[value] = atob;
                }
            }
        }

        public A this[B btoa]
        {
            get
            {
                return BToA[btoa];
            }
            set
            {
                if (!BToA.ContainsKey(btoa))
                {
                    BToA.Add(btoa, value);
                    AToB.Add(value, btoa);
                }
                else
                {
                    BToA[btoa] = value;
                    AToB[value] = btoa;
                }
            }
        }

        public Dictionary<A, B> AToB = new Dictionary<A, B>();
        Dictionary<B, A> BToA = new Dictionary<B, A>();
    }
}
