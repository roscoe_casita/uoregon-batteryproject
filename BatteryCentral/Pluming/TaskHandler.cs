﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Pluming
{
    public interface TaskHandlerInterface
    {
        TaskMaster.PostMessage Post { get; set; }
         void TaskHandlerThreadProc();
    }

    public abstract class TaskHandler : TaskHandlerInterface
    {
        public TaskHandler()
        {
            _Post += new TaskMaster.PostMessage(TaskHandler__Post);
        }
        void TaskHandler__Post(string message){}
        event TaskMaster.PostMessage _Post;

        public TaskMaster.PostMessage Post
        {
            get { return _Post; }
            set { _Post = value; }
        }

        public abstract void TaskHandlerThreadProc();
    }

    public static class TaskMaster
    {
        public delegate void PostMessage(String message);

        public static void Dispatch(List<TaskHandler> tasks, PostMessage poster)
        {
            foreach (TaskHandler th in tasks)
            {
                th.Post += poster;
                ThreadStart ts = new ThreadStart(th.TaskHandlerThreadProc);
                Thread t = new Thread(ts);
                t.IsBackground = true;
                t.Start();
            }
        }
        public static List<TaskHandler> EigenSolvers(int thread_count)
        {
            List<TaskHandler> helpers = new List<TaskHandler>();

            for(int i = 0; i< thread_count; i++)
            {
               // helpers.Add(new ComputeEigenReactions(i));
            }
            return helpers;    
        }

        
        public static List<TaskHandler> WebJSONHelers(int thread_count)
        {
            List<TaskHandler> helpers = new List<TaskHandler>();
            object[] objects = WebJSONHelper.GenerateWorkSet(thread_count);

            foreach (object s in objects)
            {
                helpers.Add(new WebJSONHelper(s));
            }
            return helpers;    
        }

        public static List<TaskHandler> DatabaseCompactors(int thread_count)
        {
            // doesn't matter thread count, this is a linear process from A-Z or not at all.

            List<TaskHandler> returnValue = new List<TaskHandler>();

            returnValue.Add(new FullScaleToCompactDatabase());
            return returnValue;
        }

        public static List<TaskHandler> QueryBuilder(int Split)
        {
            // doesn't matter thread count, this is a linear process from A-Z or not at all.

            List<TaskHandler> returnValue = new List<TaskHandler>();

            returnValue.Add(new CompactQueryBuilder());
            return returnValue;        

            
        }
    }

    public class TaskSpliter<T>
    {

        public object[] SplitTask(List<T> objects, int splitby)
        {
            List<T>[] returnValue = new List<T>[splitby];
            for (int i = 0; i < splitby; i++)
            {
                returnValue[i] = new List<T>();
            }
            int j = 0;
            foreach (T t in objects)
            {
                returnValue[j++ % splitby].Add(t);
            }
            return returnValue;
        }
    }

}
