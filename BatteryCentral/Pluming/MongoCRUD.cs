﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization;
using System.IO;
using System.Globalization;

namespace Pluming
{
    public class MongoCRUDDB
    {
        public  MongoCollection<MSGPMolecule> FullMolecules
        {
            get
            {
                return chaos.GetCollection<MSGPMolecule>("molecules");
            }
        }

        public  MongoCollection<CompactMolecule> CompactMolecules
        {
            get
            {
                return chaos.GetCollection<CompactMolecule>("compact_molecules");
            }
        }

        public  MongoCollection<CompactQuery> CompactQueries
        {
            get
            {
                return chaos.GetCollection<CompactQuery>("compact_queries");
            }
        }

        public MongoCRUDDB(string Database)
        {
            client = new MongoClient(connectionString);
            server = client.GetServer();
            chaos = server.GetDatabase(Database);
            
        }

        private  MongoDatabase chaos = null;
        private  MongoServer server = null;
        private  MongoClient client = null;
        private  string connectionString = "mongodb://localhost";
        private static  BsonClassMap<MSGPMolecule> map = BsonClassMap.RegisterClassMap<MSGPMolecule>();
        private static BsonClassMap<CompactMolecule> map3 = BsonClassMap.RegisterClassMap<CompactMolecule>();
        private static BsonClassMap<CompactQuery> map4 = BsonClassMap.RegisterClassMap<CompactQuery>();

    }

    public class ElementalCRUD
    {
        public ElementalCRUD(string Database)
        {
            MongoCRUD = new MongoCRUDDB(Database);
        }

        MongoCRUDDB MongoCRUD;

        public IEnumerable<CompactQuery> GetCompactQueries()
        {
            return MongoCRUD.CompactQueries.FindAll();
        }
        public CompactQuery GetCompactQuery(string query_name)
        {
            return MongoCRUD.CompactQueries.FindOne(Query<CompactQuery>.EQ(N => N.QueryName, query_name));
        }

        public IEnumerable<MSGPMolecule> GetMolecules()
        {
            return MongoCRUD.FullMolecules.FindAll();
        }

        public IEnumerable<MSGPMolecule> GetMolecules(IEnumerable<ObjectId> IDs)
        {
            return MongoCRUD.FullMolecules.Find(Query<MSGPMolecule>.Where(N => IDs.Contains(N.Id)));
        }

        public MSGPMolecule GetMolecule(string CIF_ID)
        {
            return MongoCRUD.FullMolecules.FindOne(Query<MSGPMolecule>.EQ(e => e.CIF, CIF_ID));
        }

        public IEnumerable<CompactMolecule> GetCompactMolecules()
        {
            return MongoCRUD.CompactMolecules.FindAll();
        }

        public void UpdateCompactMolecule(CompactMolecule cm)
        {
            MongoCRUD.CompactMolecules.Remove(Query<CompactMolecule>.EQ( e => e.Id,cm.Id));
            MongoCRUD.CompactMolecules.Insert(cm);
        }

        public void UpdateAllCompactMolecules(IEnumerable<CompactMolecule> cms)
        {
            MongoCRUD.CompactMolecules.RemoveAll();
            MongoCRUD.CompactMolecules.InsertBatch(cms);
        }

        public IEnumerable<CompactMolecule> GetCompactMolecules(CompactQuery IDs)
        {
            return MongoCRUD.CompactMolecules.Find(Query<CompactMolecule>.Where(N => IDs.CompactMolecules.Contains(N.Id)));
        }

        public IEnumerable<CompactMolecule> GetCompactMolecules(IEnumerable<ObjectId> IDS)
        {
            return MongoCRUD.CompactMolecules.Find(Query<CompactMolecule>.Where(N => IDS.Contains(N.Id)));
        }


        public CompactQuery GetCompactQuery(IEnumerable<Element> elements)
        {
            CompactQuery returnValue = null;

            foreach (CompactQuery cq in GetCompactQueries())
            {
                if (cq.Elements.ContainsAll(elements) && elements.ContainsAll(cq.Elements))
                {
                    returnValue = cq;
                    break;
                }
            }
            return returnValue;
        }

        public CompactMolecule GetCompactMolecule(string fomula_name)
        {
            return MongoCRUD.CompactMolecules.FindOne(Query<CompactMolecule>.EQ(N => N.formula, fomula_name));
        }

        public void AddMolecule(MSGPMolecule m)
        {
            MongoCRUD.FullMolecules.Insert(m);
        }

        public void AddCompactMolecule(CompactMolecule cm)
        {
            MongoCRUD.CompactMolecules.Insert(cm);
        }
        public void AddCompactQuery(CompactQuery cq)
        {
            MongoCRUD.CompactQueries.Insert(cq);
        }

        //public void DeleteMolecule(MSGPMolecule mol)
        //{
        //    MongoCRUD.FullMolecules.Remove(Query<MSGPMolecule>.EQ(e => e.CIF, mol.CIF));
        //}

        //public void DeleteCompactMolecule(CompactMolecule cm)
        //{
        //    MongoCRUD.CompactMolecules.Remove(Query<CompactMolecule>.EQ(m => m.formula, cm.formula));
        //}

        //public void DeleteCompactQuery(CompactQuery cq)
        //{
        //    MongoCRUD.CompactQueries.Remove(Query<CompactQuery>.EQ(q => q.QueryName, cq.QueryName));
        //}


        //public void UpdateMolecule(MSGPMolecule mol)
        //{
        //    DeleteMolecule(mol);
        //    AddMolecule(mol);
        //}

        //public void UpdateCompactMolecule(CompactMolecule cm)
        //{
        //    DeleteCompactMolecule(cm);
        //    AddCompactMolecule(cm);
        //}
        //public void UpdateCompactQuery(CompactQuery cq)
        //{
        //    DeleteCompactQuery(cq);
        //    AddCompactQuery(cq);
        //}
    }
}
