﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using System.Threading;


namespace Pluming
{
    public class FullScaleToCompactDatabase:TaskHandler
    {
        public override void TaskHandlerThreadProc()
        {
            ElementalCRUD CRUD = new ElementalCRUD("Chaos");
            List<CompactMolecule> Molecules = new List<CompactMolecule>();

            foreach(MSGPMolecule m in CRUD.GetMolecules())
            {
                FindAndOrAdd(ref Molecules, m);
                Post("Processed: " + m.Id);
            }

            foreach (CompactMolecule cm in Molecules)
            {
                CRUD.AddCompactMolecule(cm);

                Post("Added Compact Molecule: " + cm.formula);
            }
        }




        private static void FindAndOrAdd(ref List<CompactMolecule> molecules, MSGPMolecule molecule)
        {
            foreach (CompactMolecule cm in molecules)
            {
                if (cm.NumberElements == molecule.NElements)
                {
                    if (cm.formula.Length == molecule.PrettyFormula.Length)
                    {
                        if (cm.formula.CompareTo(molecule.PrettyFormula) == 0)
                        {
                            cm.MSGPMolecules = cm.MSGPMolecules.Concat(new[] { molecule.Id }).ToArray();
                            return;
                        }
                    }
                }
            }
            CompactMolecule new_cm = CompactMolecule.CMFromMSGPMolecule(molecule);
            molecules.Add(new_cm);
            return;
        }
    }

    public class CompactQueryBuilder:TaskHandler
    {
        public override void TaskHandlerThreadProc()
        {
            ElementalCRUD CRUD = new ElementalCRUD("Chaos");
            List<CompactQuery> Queries = new List<CompactQuery>();

            foreach (CompactMolecule cm in CRUD.GetCompactMolecules())
            {
                AddOrCreate(ref Queries, cm);
            }

            Queries.Sort();

            for(int i = 0; i< Queries.Count; i++)
            {
                for (int j = 0; j < Queries.Count; j++)
                {
                    if (i != j)
                    {
                        TestAdd(Queries[i],Queries[j]);
                    }
                }
            }

            foreach (CompactQuery cq in Queries)
            {
                CRUD.AddCompactQuery(cq);
                Post("Inserted Query: " + cq.QueryName);
            }

        }

        private void TestAdd(CompactQuery compactQuery, CompactQuery compactQuery_2)
        {
            foreach (Element e in compactQuery.Elements)
            {
                if (!compactQuery_2.Elements.Contains(e))
                {
                    return;
                }
            }
            Post("Added Query :" + compactQuery.QueryName + " to " + compactQuery_2.QueryName);
            compactQuery_2.CompactMolecules =
                compactQuery_2.CompactMolecules.Concat(compactQuery.UniqueCompactMolecules).ToArray();

            compactQuery_2.CompactMoleculeCount += compactQuery.UniqueMoleculeCount;

        }



        private void AddOrCreate(ref List<CompactQuery> Queries, CompactMolecule cm)
        {
            string s = ElementHelper.ElementsToString(cm.Elements);

            CompactQuery cq = null;
            foreach (CompactQuery test in Queries)
            {
                if (test.QueryName.Length == s.Length)
                {
                    if (test.QueryName.CompareTo(s) == 0)
                    {
                        cq = test;
                        break;
                    }
                }
            }

            if (cq == null)
            {
                cq = new CompactQuery();
                cq.QueryName = s;
                cq.Id = ObjectId.GenerateNewId();
                List<Element> temp = cm.Elements.ToList();
                temp.Sort();
                cq.Elements = temp.ToArray();
                cq.ElementCount = cm.Elements.Length;
                cq.CompactMolecules = new ObjectId[1];
                cq.CompactMolecules[0] = cm.Id;
                cq.CompactMoleculeCount = 1;
                cq.UniqueCompactMolecules = new ObjectId[1];
                cq.UniqueCompactMolecules[0] = cm.Id;
                cq.UniqueMoleculeCount = 1;
                Queries.Add(cq);
                Post("Created Query: " + s);
            }
            else
            {
                cq.CompactMolecules = cq.CompactMolecules.Concat(new[] { cm.Id }).ToArray();
                cq.CompactMoleculeCount++;
                cq.UniqueCompactMolecules = cq.UniqueCompactMolecules.Concat(new[] { cm.Id }).ToArray();
                cq.UniqueMoleculeCount++;
            }

        }

    }
}
