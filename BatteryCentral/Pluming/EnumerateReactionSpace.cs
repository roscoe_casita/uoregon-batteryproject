﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MathNet;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Numerics;

namespace Pluming
{
    public class EnumerateReactionSpace
    {


        public EnumerateReactionSpace(IEnumerable<CompactMolecule> compounds)
        {
            List<CompactMolecule> sortedList = compounds.ToList();

            sortedList.Sort();
            sortedList.Reverse();
            //sortedList = Shuffle(sortedList);

            IndexEnumerator = new IndexStackCounter<CompactMolecule>(sortedList);
        }

        public IEnumerable<Reaction> EnumerateAllReactions()
        {
            PrimodialCount = 0;
            LinearCount = 0;
            UniqueReactions = 0;
            Enumerations = 0;
            DiveOps = 0;
            AdvanceOps = 0;
            UnwindOps = 0;
            bool done = false;

            while (!done)
            {
                List<CompactMolecule> molecules = IndexEnumerator.EnumerateCurrentIndex.ToList();
                CompoundReactions cr = new CompoundReactions(molecules);

                Reaction[] reactions = cr.EnumerateReactions().ToArray();
                Enumerations++;

                switch (reactions.Length)
                {
                    case 0:
                        PrimodialCount++;
                        if (IndexEnumerator.Dive())
                        {
                            DiveOps++;
                            done = false;
                        }
                        else
                        {
                            if (IndexEnumerator.Advance())
                            {
                                AdvanceOps++;
                                done = false;
                            }
                            else
                            {
                                if (IndexEnumerator.Unwind())
                                {
                                    UnwindOps++;
                                    if (IndexEnumerator.Advance())
                                    {
                                        AdvanceOps++;
                                        done = false;
                                    }
                                    else
                                    {
                                        done = true;
                                    }
                                }
                                else
                                {
                                    done = true;
                                }
                            }
                        }
                        break;
                    case 1:

                        bool add_reaction = true;

                        foreach (CompactMolecule cm in molecules)
                        {
                            if (!reactions[0].ContainsMolecule(cm))
                            {
                                add_reaction = false;
                                break;
                            }
                        }

                        if (add_reaction)
                        {
                            UniqueReactions++;
                            yield return reactions[0];
                        }
                        else
                        {
                            LinearCount++;
                        }
                        if (IndexEnumerator.Advance())
                        {
                            AdvanceOps++;
                            done = false;
                        }
                        else
                        {
                            if (IndexEnumerator.Unwind())
                            {
                                UnwindOps++;
                                if (IndexEnumerator.Advance())
                                {
                                    AdvanceOps++;
                                    done = false;
                                }
                                else
                                {
                                    done = true;
                                }
                            }
                            else
                            {
                                done = true;
                            }
                        }
                        break;
                    default:
                        LinearCount++;
                        if (IndexEnumerator.Unwind())
                        {
                            UnwindOps++;
                            if (IndexEnumerator.Advance())
                            {
                                AdvanceOps++;
                                done = false;
                            }
                            else
                            {
                                done = true;
                            }
                        }
                        else
                        {
                            done = true;
                        }
                        break;
                }
            }
            yield break;
        }


        IndexStackCounter<CompactMolecule> IndexEnumerator = null;

        public int TotalReactionCount { get { return PrimodialCount + LinearCount + UniqueReactions; } }

        public int PrimodialCount { get; set; }

        public int LinearCount { get; set; }

        public int UniqueReactions { get; set; }

        public int Enumerations     { get; set; }
        public int DiveOps          { get; set; }
        public int AdvanceOps           { get; set; }
        public int UnwindOps        { get; set; }

        public static string GenerateGraphVizReactions(IEnumerable<Reaction> reactions,ElementalCRUD CRUD)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("digraph \"Unique Reaction Hypergraph\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"Unique Reaction Hypergraph\",");
            sb.AppendLine("size = \"12,12\"];");

            List<CompactMolecule> mols = new List<CompactMolecule>();

            foreach (Reaction r in reactions)
            {
                foreach (var v in r.EnumerateReactions)
                {
                    if (!mols.Contains(v.Key))
                    {
                        mols.Add(v.Key);
                    }
                }
            }
            foreach (CompactMolecule cm in mols)
            {
                sb.AppendLine(cm.GetHashCode() + " [label=\"" + cm.formula +"\" fillcolor=\"green:yellow\" style=\"filled\" gradientangle=90];");
            }

            foreach(Reaction r in reactions)
            {
                double counter = 0.0;
                double left_count = 0.0;
                double right_count = 0.0;

                List<CompactMolecule> left_side = new List<CompactMolecule>();
                List<CompactMolecule> right_side = new List<CompactMolecule>();
                List<CompactMolecule> temp = null;

                foreach (KeyValuePair<CompactMolecule,int> cm in r.EnumerateReactions)
                {
                    counter += cm.Value * cm.Key.energy * (-1);

                    if (cm.Value > 0)
                    {
                        left_side.Add(cm.Key);
                        left_count += Math.Abs(cm.Value) * cm.Key.energy;
                    }
                    else
                    {
                        right_side.Add(cm.Key);
                        right_count += Math.Abs(cm.Value) * cm.Key.energy;
                    }                    
                }

                counter = Math.Round(counter);


                sb.AppendLine(r.GetHashCode() + " [label=\""+ counter +" kJ\" shape=circle fillcolor=\"red:blue\" style=\"filled\" gradientangle=90];");

                if (left_count < right_count)
                {
                    temp = left_side;
                    left_side = right_side;
                    right_side = temp;
                }

                foreach(CompactMolecule cm in left_side)
                {
                    sb.AppendLine(cm.GetHashCode() + " -> " + r.GetHashCode() + " [label =\"" + Math.Abs(r.GetCoeffecient(cm)) + "\" color=\"blue\"];");
                }
                foreach(CompactMolecule cm in right_side)
                {
                    sb.AppendLine(r.GetHashCode() + " -> " + cm.GetHashCode() + " [label =\"" + Math.Abs(r.GetCoeffecient(cm)) + "\" color=\"red\"];");
                }
            
            }

            sb.Append("}\r\n");

            return sb.ToString();
        }
    }

    public class CompoundReactions
    {
        public CompoundReactions(IEnumerable<CompactMolecule> compounds)
        {
            foreach (CompactMolecule cm in compounds)
            {
                CompoundNames.Add(cm.formula, cm);
            }
            // ok we now have all the 'atom' strings to index map. for each row decomposition
            // And we have a molecule to column decomposition.

            RowMap = EquationHelper.CreateRowMap(compounds);
            ColumnMap = EquationHelper.CreateColumnMap(compounds);
            Matrix = EquationHelper.BuildMatrix(RowMap, ColumnMap);
            SolveMatrix();
        }


        public IEnumerable<Reaction> EnumerateReactions()
        {
            List<Reaction> reactions = new List<Reaction>();

            Rational[] vector = new Rational[ColumnMap.Count];

            
            for (int c = 0; c < ColumnMap.Count; c++)
            {
                bool Positive = false;
                bool Negative = false;
                for (int r = 0; r < RowMap.Count; r++)
                {
                    if (r != c)
                    {
                        if(Matrix[r, c].IsPositive)
                        {
                            Positive = true;
                        }
                        if (Matrix[r, c].IsNegative)
                        {
                            Negative = true;
                        }
                    }

                }
                if (Positive == false && Negative == false)
                    continue;

                for (int v = 0; v < vector.Length; v++)
                {
                    if (v == c)
                    {
                        if (Negative == false)
                        {
                            vector[v] = new Rational(-1);
                        }
                        else
                        {
                            vector[v] = new Rational(1);
                        }
                    }
                    else if (v < RowMap.Count)
                    {
                        vector[v] = new Rational(Matrix[v, c].Numerator, Matrix[v, c].Denominator);
                    }
                    else
                    {
                        vector[v] = new Rational(0);
                    }
                }

                Rational[] temp = BalanceVector(vector);

                Reaction react = GenerateReactionFromVector(temp);

                if (react.Validate())
                {
                    reactions.Add(react);
                }
                else
                {
                    vector[c] = new Rational(vector[c].Numerator * -1, vector[c].Denominator);
                    temp = BalanceVector(vector);
                    react = GenerateReactionFromVector(temp);
                    if (react.Validate())
                    {
                        reactions.Add(react);
                    }
                    else
                    {
                       // throw new Exception("Generated non-identity, non-balanced equation.");
                    }
                }
            }

            return reactions;
        }

        private Reaction GenerateReactionFromVector(Rational[] vector)
        {
            Dictionary<CompactMolecule, int> equation = new Dictionary<CompactMolecule, int>();

            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i].Denominator != 1)
                {
                    throw new Exception("Bad Denominator!");
                }
                int coeffecient = (int)vector[i].Numerator;
                CompactMolecule cm = ColumnMap[i];

                if (coeffecient != 0)
                {
                    equation.Add(cm, coeffecient);
                }
            }
            return new Reaction(equation);
        }

        private static Rational[] BalanceVector(Rational[] vector_in)
        {
            Rational[] vector = new Rational[vector_in.Length];
            for (int i = 0; i < vector_in.Length; i++)
            {
                vector[i] = new Rational(vector_in[i].Numerator, vector_in[i].Denominator);
            }

            bool done = false;
            int index = 0;
            while (!done)
            {
                BigInteger mul = BigInteger.One;

                for (int i = 0; i < vector.Length; i++)
                {
                    if (mul == BigInteger.One)
                    {
                        mul = vector[i].Denominator;
                    }
                    else
                    {
                        
                        if( (vector[i].Denominator != BigInteger.One) &&
                            (vector[i].Denominator < mul))
                        {
                            mul = vector[i].Denominator;
                        }
                    }
                }

                if (index >= vector.Length || mul == 1)
                {
                    done = true;
                    break;
                }
                else
                {
                    Rational multiply = new Rational(mul, BigInteger.One);
                    for (int i = 0; i < vector.Length; i++)
                    {
                        vector[i] = vector[i] * multiply;
                    }
                }
            }
            return vector;
        }

        private void SolveMatrix()
        {
            Solver.rref(Matrix);
        }

        public Rational[,] Matrix = null;
        Map<string, int> RowMap = null;
        Map<CompactMolecule, int> ColumnMap = null;

        private Dictionary<string, CompactMolecule> CompoundNames = new Dictionary<string, CompactMolecule>();


        private static class EquationHelper
        {

            public static Rational[,] BuildMatrix(Map<string, int> RowMap, Map<CompactMolecule, int> ColumnMap)
            {
                Rational[,] M = new Rational[RowMap.Count, ColumnMap.Count];
                int col = 0;
                foreach (IEnumerable<Rational> column_of_rows in MakeMatrix(RowMap, ColumnMap))
                {
                    int row = 0;
                    foreach (Rational r in column_of_rows)
                    {
                        M[row, col] = r;
                        row++;
                    }
                    col++;
                }
                return M;
            }

            private static IEnumerable<IEnumerable<Rational>> MakeMatrix(Map<string, int> RowMap, Map<CompactMolecule, int> ColumnMap)
            {
                for (int i = 0; i < ColumnMap.Count; i++)
                {
                    CompactMolecule m = ColumnMap[i];

                    yield return MakeColumn(m, RowMap);
                }
            }

            private static IEnumerable<Rational> MakeColumn(CompactMolecule compound, Map<string, int> RowMap)
            {
                for (int i = 0; i < RowMap.Count; i++)
                {
                    string e = RowMap[i];
                    Element e1 = ElementHelper.StringToElements(e).First();

                    if (compound.Elements.Contains(e1))
                    {
                        yield return new Rational((int)compound.UnitCellFormula[e1]);
                    }
                    else
                    {
                        yield return new Rational(0);
                    }
                }
            }

            public static Map<CompactMolecule, int> CreateColumnMap(IEnumerable<CompactMolecule> compounds)
            {
                int count = 0;
                Map<CompactMolecule, int> returnValue = new Map<CompactMolecule, int>();

                foreach (CompactMolecule c in compounds)
                {
                    if (!returnValue.Contains(c))
                    {
                        returnValue[c] = count++;
                    }
                }
                return returnValue;
            }

            public static Map<string, int> CreateRowMap(IEnumerable<CompactMolecule> compounds)
            {
                int count = 0;
                Map<string, int> returnValue = new Map<string, int>();

                foreach (CompactMolecule c in compounds)
                {
                    foreach (Element e in c.Elements)
                    {
                        if (!returnValue.Contains(e.ToString()))
                        {
                            returnValue[e.ToString()] = count++;
                        }
                    }
                }
                return returnValue;
            }
        }
    }
}
