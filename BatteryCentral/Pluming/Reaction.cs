﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluming
{

    public class Reaction
    {
        public Reaction(Dictionary<CompactMolecule, int> equation)
        {
            Equation = equation;
        }

        public IEnumerable<KeyValuePair<CompactMolecule, int>> EnumerateReactions
        {
            get
            {
                foreach (KeyValuePair<CompactMolecule, int> kvp in Equation)
                {
                    yield return kvp;
                }
            }
        }
        public bool Validate()
        {
            bool negativeValue = false;
            bool positiveValue = false;
            foreach (KeyValuePair<CompactMolecule, int> kvp in Equation)
            {
                if (kvp.Value < 0)
                {
                    negativeValue = true;
                }
                if (kvp.Value > 0)
                {
                    positiveValue = true;
                }
            }
            if (negativeValue == false || positiveValue == false)
            {
                return false;
            }
            Dictionary<Element, int> counter = new Dictionary<Element, int>();

            foreach (KeyValuePair<CompactMolecule, int> kvp in Equation)
            {
                foreach(Element e in kvp.Key.Elements)
                {
                    if(!counter.ContainsKey(e))
                    {
                        counter.Add(e,0);
                    }
                }
                foreach (KeyValuePair<Element, int> count in kvp.Key.UnitCellFormula)
                {
                    int element_count = kvp.Value * count.Value;

                    counter[count.Key] += element_count;
                }
            }
            foreach (KeyValuePair<Element, int> kvp in counter)
            {
                if (kvp.Value != 0)
                {
                    return false;
                }
            }
            return true;
        }
        public bool ContainsMolecule(CompactMolecule cm)
        {
            foreach (CompactMolecule mol in Equation.Keys)
            {
                if (mol.CompareTo(cm) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        public int GetCoeffecient(CompactMolecule cm)
        {
            return Equation[cm];
        }
        Dictionary<CompactMolecule, int> Equation = null;

        public string DisplayString(ElementalCRUD CRUD) //ElementalCRUD CRUD)
        {
            StringBuilder products = new StringBuilder();
            StringBuilder reactants = new StringBuilder();

            string add = " + ";
            foreach (KeyValuePair<CompactMolecule, int> kvp in Equation)
            {
                StringBuilder add_to = null;
                if (kvp.Value < 0)
                {
                    add_to = reactants;
                }
                else
                {
                    add_to = products;
                }
                add_to.AppendFormat("{0}*{1}{2}", (kvp.Value<0?kvp.Value*-1:kvp.Value), kvp.Key.formula, add);
            }
            products.Remove(products.Length - add.Length, add.Length);
            reactants.Remove(reactants.Length - add.Length, add.Length);
            return reactants.ToString() + " = " + products.ToString();
        }
    }
}
