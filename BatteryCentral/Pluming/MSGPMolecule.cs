﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;

namespace Pluming
{
    /// These classes all allow JSON from the materials science project to be imported into a mongoDB.


    public class FullQuery
    {
        public ObjectId Id { get; set; }

        [BsonElement("query_name")]
        public string QueryName { get; set; }

        [BsonElement("contained_queries")]
        public List<ObjectId> ContainedQueries { get; set; }

        [BsonElement("created_at")]
        public string CreatedAt { get; set; }

        [BsonElement("valid_response")]
        public bool ValidResponse { get; set; }

        [BsonElement("version")]
        public VersionInfo Version { get; set; }

        [BsonElement("response")]
        public List<MSGPMolecule> Molecules { get; set; }

        [BsonElement("copyright")]
        public string CopyRight { get; set; }
    }

    public class VersionInfo
    {
        [BsonElement("pymatgen")]
        public string PYMATGEN { get; set; }

        [BsonElement("db")]
        public string Database { get; set; }

        [BsonElement("rest")]
        public string Rest { get; set; }
    }

    public class SpaceGroup
    {
        [BsonElement("symbol")]
        public string Symbol { get; set; }

        [BsonElement("number")]
        public int Number{ get; set; }

        [BsonElement("point_group")]
        public string PointGroup { get; set; }

        [BsonElement("source")]
        public string Source { get; set; }

        [BsonElement("crystal_system")]
        public string CrystalSystem { get; set; }

        [BsonElement("hall")]
        public string Hall { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine( "        Symbol:          " + Symbol);
            sb.AppendLine( "        Number:          " + Number);
            sb.AppendLine( "        PointGroup:      " + PointGroup);
            sb.AppendLine( "        Source:          " + Source);
            sb.AppendLine( "        CrystalSystem:   " + CrystalSystem);
            sb.AppendLine( "        Hall:            " + Hall);

            return sb.ToString();
            
        }

    }

    public class MSGPMolecule:IComparable<MSGPMolecule>
    {
        public ObjectId Id { get; set; }

        [BsonElement("full_formula")]
        public string FullFormula = "";

        [BsonElement("pretty_formula")]
        public string PrettyFormula = "";

        [BsonElement("material_id")]
        public string MaterialID = "";

        [BsonElement("oxide_type")]
        public string OxideType = "";

        [BsonElement("cif")]
        public string CIF = "";

        [BsonElement("is_hubbard")]
        public bool IsHubbard = false;

        [BsonElement("is_compatible")]
        public bool IsCompatible = false;

        [BsonElement("nelements")]
        public int NElements = 0;

        [BsonElement("nsites")]
        public int NSites = 0;

        [BsonElement("energy")]
        public double Energy = 0.0;

        [BsonElement("band_gap")]
        public double BandGap = 0.0;

        [BsonElement("e_above_hull")]
        public double EAboveHull = 0.0;

        [BsonElement("volume")]
        public double Volume = 0.0;

        [BsonElement("total_magnetization")]
        public double TotalMagnetization = 0.0;

        [BsonElement("density")]
        public double Density = 0.0;

        [BsonElement("energy_per_atom")]
        public double EnergyPerAtom = 0.0;

        [BsonElement("formation_energy_per_atom")]
        public double FormationEnergyPerAtom = 0.0;

        [BsonElement("icsd_ids")]
        public int[] ICSD_IDS { get; set; }

        [BsonElement("unit_cell_formula")]
        public Dictionary<String, decimal> UnitCellFormula { get; set; } // = new Dictionary<string, decimal>();

        [BsonElement("hubbards")]
        public Dictionary<String, decimal> Hubbards { get; set; } // = new Dictionary<string, decimal>();

        [BsonElement("spacegroup")]
        public SpaceGroup Space_Group { get; set; } // = new Dictionary<string, string>();

        [BsonElement("task_ids")]
        public string[] TaskIDS { get; set; } //= new List<string>();

        [BsonElement("icsd_id")]
        public int ICSD_ID { get; set; } //= new List<string>();

        [BsonElement("elements")]
        public string[] Elements { get; set; } // = new List<string>();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("PrettyFormula:           " + this.PrettyFormula);
            sb.AppendLine("FullFormula:             " + this.FullFormula);
            sb.AppendLine("BandGap:                 " + this.BandGap);
            sb.AppendLine("Density:                 " + this.Density);
            sb.AppendLine("EAboveHull:              " + this.EAboveHull);
            sb.AppendLine("Energy:                  " + this.Energy);
            sb.AppendLine("EnergyPerAtom:           " + this.EnergyPerAtom);
            sb.AppendLine("FormationEnergyPerAtom:  " + this.FormationEnergyPerAtom);
            sb.AppendLine("TotalMagnetization:      " + this.TotalMagnetization);
            sb.AppendLine("Volume:                  " + this.Volume);
            sb.AppendLine("OxideType:               " + this.OxideType);
            sb.AppendLine("NSites:                  " + this.NSites);
            sb.AppendLine("Space_Group:             " );
            sb.AppendLine(this.Space_Group.ToString());
            sb.AppendLine("Elements:                " );

            foreach (KeyValuePair<string, Decimal> kvp in this.UnitCellFormula)
            {
           sb.AppendFormat("{0,15}:{1,15}\r\n", kvp.Key, (int)kvp.Value);
            }


            return sb.ToString();
            
        }

        public int CompareTo(MSGPMolecule other)
        {
            int i = FullFormula.CompareTo(other.FullFormula);
            if (i != 0)
            {
                return i;
            }
            return CIF.CompareTo(other.CIF);
        }
    }

    //public class MolecularQuery: IComparable<MolecularQuery>
    //{
    //    public ObjectId Id { get; set; }

    //    [BsonElement("query_name")]
    //    public string QueryName { get; set; }

    //    [BsonElement("molecules")]
    //    public ObjectId[] MoleculeIDs { get; set; }

    //    [BsonElement("molecule_count")]
    //    public int MoleculeCount { get; set; }

    //    [BsonElement("unique_molecules")]
    //    public ObjectId[] UniqueMoleculeIDs { get; set; }

    //    [BsonElement("unique_molecule_count")]
    //    public int UniqueMoleculeCount { get; set; }


    //    [BsonElement("elements")]
    //    public Element[] Elements { get; set; }

    //    [BsonElement("number_elements")]
    //    public int NumberOfElements { get; set; }
        
    //    public int CompareTo(MolecularQuery other)
    //    {
    //       return QueryName.CompareTo(other.QueryName);
    //    }
    //}


    public class CompactMolecule: IComparable<CompactMolecule>
    {
        public ObjectId Id { get; set; }

        public IEnumerable<MSGPMolecule> GetMolecules(ElementalCRUD CRUD)
        {
            foreach (MSGPMolecule mol in CRUD.GetMolecules(MSGPMolecules))
            {
                yield return mol;
            }
        }

        [BsonElement("linked_molecules")]
        public ObjectId[] MSGPMolecules { get; set; }

        [BsonElement("elements")]
        public Element[] Elements { get; set; }

        [BsonElement("number_elements")]
        public int NumberElements = 0;

        [BsonElement("formula")]
        public string formula = "";

        [BsonElement("unit_cell_formula")]
        public Dictionary<Element, int> UnitCellFormula { get; set; } // = new Dictionary<string, decimal>();

        public int CompareTo(CompactMolecule other)
        {
            if (NumberElements == other.NumberElements)
            {
                return formula.CompareTo(other.formula);
            }
            else
            {
                return NumberElements.CompareTo(other.NumberElements);
            }
        }
        public static CompactMolecule CMFromMSGPMolecule(MSGPMolecule molecule)
        {
            CompactMolecule cm = new CompactMolecule();
            cm.MSGPMolecules = new ObjectId[1];
            cm.MSGPMolecules[0] = molecule.Id;
            cm.Elements = ElementHelper.StringsToElements(molecule.Elements).ToArray();
            cm.NumberElements = molecule.NElements;
            cm.formula = (string)molecule.PrettyFormula.Clone();
            cm.UnitCellFormula = new Dictionary<Element, int>();
            foreach (KeyValuePair<string, decimal> kvp in molecule.UnitCellFormula)
            {
                cm.UnitCellFormula.Add((Element)Enum.Parse(typeof(Element),kvp.Key), (int)kvp.Value);
            }
            cm.Id = ObjectId.GenerateNewId();
            return cm;
        }

        [BsonElement("energy")]
        public double energy { get; set; }

        [BsonElement("density")]
        public double density { get; set; }

        [BsonElement("volume")]
        public double volume { get; set; }

        [BsonElement("eenergy")]
        public double eenergy { get; set; }

        [BsonElement("band_gap")]
        public double band_gap { get; set; }
    }

    public class CompactQuery:IComparable
    {
        public ObjectId Id { get; set; }

        [BsonElement("query_name")]
        public string QueryName { get; set; }

        [BsonElement("elements")]
        public Element[] Elements { get; set; }

        [BsonElement("element_count")]
        public int ElementCount { get; set; }

        [BsonElement("compact_molecule_count")]
        public int CompactMoleculeCount { get; set; }

        [BsonElement("compact_molecules")]
        public ObjectId[] CompactMolecules { get; set; }

        [BsonElement("unique_compact_molecule_count")]
        public int UniqueMoleculeCount { get; set; }

        [BsonElement("unique_compact_molecules")]
        public ObjectId[] UniqueCompactMolecules { get; set; }


        public int CompareTo(object obj)
        {
            CompactQuery cq = (CompactQuery)obj;

            if (ElementCount != cq.ElementCount)
            {
                return ElementCount.CompareTo(cq.ElementCount);
            }
            else
            {
                for (int i = 0; i < ElementCount; i++)
                {
                    int r = Elements[i].CompareTo(cq.Elements[i]);
                    if (r != 0)
                    {
                        return r;
                    }
                }
            }
            return 0;
        }

        
    }
}
