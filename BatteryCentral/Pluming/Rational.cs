﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace Pluming
{
    public class Rational : IComparable<Rational>, IEquatable<Rational>
    {
        public override int GetHashCode()
        {
            return (_Denominator.GetHashCode() ^ _Numerator.GetHashCode()).GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.CompareTo((Rational)obj) == 0;
        }
        public Rational() { _Numerator = 0; _Denominator = 1; }

        public Rational(int numerator) { _Numerator = numerator; _Denominator = 1; }

        public Rational(BigInteger numerator) { _Numerator = numerator; }

        public Rational(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new Exception("WRONG: Denominator is Zero.");
            }           
            
            BigInteger gcd = BigInteger.GreatestCommonDivisor(numerator, denominator);
            _Numerator = numerator / gcd;
            _Denominator = denominator / gcd;

            if (denominator < 0)
            {
                _Numerator = _Numerator * -1;
                _Denominator = _Denominator * -1;
            }
        }

        public Rational(BigInteger numerator, BigInteger denominator)
        {
            if (denominator == 0)
            {
                throw new Exception("WRONG: Denominator is Zero.");
            }          
            
            BigInteger gcd = BigInteger.GreatestCommonDivisor(numerator, denominator);
            _Numerator = numerator / gcd;
            _Denominator = denominator / gcd;

            if (denominator < 0)
            {
                _Numerator = _Numerator * -1;
                _Denominator = _Denominator * -1;
            }
        }

        public BigInteger Numerator { get { return _Numerator; } }
        public BigInteger Denominator { get { return _Denominator; } }

        public bool IsWhole { get { return _Denominator == 1; } }
        public bool IsZero { get { return _Denominator == 1 && _Numerator == 0; } }
        public bool IsPositiveOne { get { return _Denominator == 1 && _Numerator == 1; } }
        public bool IsNegativeOne { get { return _Denominator == 1 && _Numerator == -1; } }
        public bool IsNegative { get { return _Numerator < 0; } }
        public bool IsPositive { get { return _Numerator > 0; } }

        public Rational Inverse { get { return new Rational(_Denominator, _Numerator); } }

        public static Rational operator +(Rational r1, Rational r2)
        {
            return new Rational(r1.Numerator * r2.Denominator + r2.Numerator * r1.Denominator, r1.Denominator * r2.Denominator);
        }

        public static Rational operator -(Rational r1, Rational r2)
        {

            return new Rational(r1.Numerator * r2.Denominator - r2.Numerator * r1.Denominator, r1.Denominator * r2.Denominator);
        }

        public static Rational operator *(Rational r1, Rational r2)
        {
            return new Rational(r1.Numerator * r2.Numerator, r1.Denominator * r2.Denominator);
        }

        public static Rational operator /(Rational r1, Rational r2)
        {
            return new Rational(r1.Numerator * r2.Denominator, r1.Denominator * r2.Numerator);
        }

        public int CompareTo(Rational other)
        {
            int nValue = Numerator.CompareTo(other.Numerator);
            int dValue = Denominator.CompareTo(other.Denominator);
            if ((nValue == 0) && (dValue == 0))
            {
                return 0;
            }
            if (dValue == 0)
            {
                return nValue;
            }
            return dValue * nValue;
        }

        public override string ToString()
        {
            if ((_Denominator == 1) || (_Numerator == 0))
            {
                return _Numerator.ToString("N0");
            }
            else
            {
                return _Numerator + "/" + _Denominator;
            }
        }

        private readonly BigInteger _Numerator;
        private readonly BigInteger _Denominator;

        public bool Equals(Rational other)
        {
            return (_Numerator == other._Numerator) == (_Denominator == other.Denominator);
        }

        int IComparable<Rational>.CompareTo(Rational other)
        {
            int returnCode = other._Denominator.CompareTo(_Denominator);
            if (returnCode == 0)
                returnCode = other._Numerator.CompareTo(_Numerator);
            return returnCode;
        }

        bool IEquatable<Rational>.Equals(Rational other)
        {
            if (this.CompareTo(other) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //public static Rational NegativeOne()
        //{
        //    return new Rational(BigInteger.MinusOne, BigInteger.One);
        //}
        //public static Rational PositiveOne()
        //{
        //    return new Rational(BigInteger.One, BigInteger.One);
        //}
    }
}
