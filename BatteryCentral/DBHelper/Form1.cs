﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Numerics;
using Pluming;

namespace DBHelper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ElementalCRUD CRUD = new ElementalCRUD("Chaos");
          
        private void button1_Click(object sender, EventArgs e)
        {

            CompactMolecule [] compounds = CRUD.GetCompactMolecules().ToArray();
            Dictionary<Element, List<CompactMolecule>> count_test = new Dictionary<Element, List<CompactMolecule>>();
            
            foreach(Element element in ElementHelper.AllElements)
            {
                count_test.Add(element, new List<CompactMolecule>());

                for (int i = 0; i < compounds.Length; i++)
                {
                    CompactMolecule cm = compounds[i];

                    if(cm.Elements.Contains(element))
                    {
                        count_test[element].Add(cm);
                    }
                }      
            }

            BigInteger bi = 1;
            BigInteger bo = 1;

            foreach (Element element in ElementHelper.AllElements)
            {
                if (count_test[element].Count > 0)
                {
                    bi = bi * count_test[element].Count;

                    if ((element != Element.H) &&
                        (element != Element.O) &&
                        (element != Element.N))
                        bo = bo * count_test[element].Count;
                }
            }

            BigInteger universe = BigInteger.Pow(2,100);
            richTextBox1.Text = "Size of Universe: " + universe.ToString() + "\r\n";
            richTextBox1.Text += "Size of Reduced Problem: " + bi.ToString() +"\r\n";
            richTextBox1.Text += "Size of Second Reduced Problem: " + bo.ToString() +"\r\n";
        }

        //private void test()
        //{

        //    for (int i = 0; i < compounds.Length; i++)
        //    {
        //        MSGPMolecule[] molecules = compounds[i].GetMolecules(CRUD).ToArray();

        //        MSGPMolecule select = molecules[0];
        //        string element = select.Elements[0];
        //        for (int j = 1; j< molecules.Length; j++)
        //        {
        //            if (molecules[j].PrettyFormula == molecules[j].FullFormula)
        //            {
        //                select = molecules[j];
        //                break;
        //            }
        //            if (select.UnitCellFormula[element] > molecules[j].UnitCellFormula[element])
        //            {
        //                select = molecules[j];
        //            }

        //        }
        //        compounds[i].formula = select.FullFormula;
        //        compounds[i].UnitCellFormula.Clear();
        //        foreach (KeyValuePair<string, decimal> kvp in select.UnitCellFormula)
        //        {
        //            Element elemental = ElementHelper.StringToElement(kvp.Key);
        //            int count = (int)kvp.Value;
        //            compounds[i].UnitCellFormula.Add(elemental, count);
        //        }
        //        compounds[i].energy = select.Energy;
        //        compounds[i].density = select.Density;
        //        compounds[i].volume = select.Volume;
        //        compounds[i].eenergy = select.EAboveHull;
        //        compounds[i].band_gap = select.BandGap;

        //    }
        //    CRUD.UpdateAllCompactMolecules(compounds);
        //}
    }
}
