﻿namespace TestingHarness
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ElementsToQueries = new System.Windows.Forms.Button();
            this.CompoundsToReactions = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.ElementBox = new System.Windows.Forms.CheckedListBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.CompoundBox = new System.Windows.Forms.CheckedListBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ElementsToQueries
            // 
            this.ElementsToQueries.Location = new System.Drawing.Point(11, 13);
            this.ElementsToQueries.Name = "ElementsToQueries";
            this.ElementsToQueries.Size = new System.Drawing.Size(207, 38);
            this.ElementsToQueries.TabIndex = 0;
            this.ElementsToQueries.Text = "Elements - to - Queries";
            this.ElementsToQueries.UseVisualStyleBackColor = true;
            this.ElementsToQueries.Click += new System.EventHandler(this.button1_Click);
            // 
            // CompoundsToReactions
            // 
            this.CompoundsToReactions.Location = new System.Drawing.Point(18, 30);
            this.CompoundsToReactions.Name = "CompoundsToReactions";
            this.CompoundsToReactions.Size = new System.Drawing.Size(214, 38);
            this.CompoundsToReactions.TabIndex = 2;
            this.CompoundsToReactions.Text = "Compounds-to-Reactions";
            this.CompoundsToReactions.UseVisualStyleBackColor = true;
            this.CompoundsToReactions.Click += new System.EventHandler(this.CompoundsToReactions_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(18, 100);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(230, 38);
            this.button4.TabIndex = 3;
            this.button4.Text = "Calculate Reaction";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ElementBox
            // 
            this.ElementBox.FormattingEnabled = true;
            this.ElementBox.Location = new System.Drawing.Point(25, 61);
            this.ElementBox.Name = "ElementBox";
            this.ElementBox.Size = new System.Drawing.Size(207, 466);
            this.ElementBox.TabIndex = 5;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox2.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(0, 0);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(572, 739);
            this.richTextBox2.TabIndex = 6;
            this.richTextBox2.Text = "";
            // 
            // CompoundBox
            // 
            this.CompoundBox.FormattingEnabled = true;
            this.CompoundBox.Location = new System.Drawing.Point(18, 171);
            this.CompoundBox.Name = "CompoundBox";
            this.CompoundBox.Size = new System.Drawing.Size(214, 424);
            this.CompoundBox.TabIndex = 7;
            this.CompoundBox.SelectedIndexChanged += new System.EventHandler(this.CompoundBox_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox2);
            this.splitContainer1.Size = new System.Drawing.Size(1104, 739);
            this.splitContainer1.SplitterDistance = 528;
            this.splitContainer1.TabIndex = 8;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.ElementBox);
            this.splitContainer2.Panel1.Controls.Add(this.ElementsToQueries);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.CompoundBox);
            this.splitContainer2.Panel2.Controls.Add(this.button4);
            this.splitContainer2.Panel2.Controls.Add(this.CompoundsToReactions);
            this.splitContainer2.Size = new System.Drawing.Size(528, 739);
            this.splitContainer2.SplitterDistance = 235;
            this.splitContainer2.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 739);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ElementsToQueries;
        private System.Windows.Forms.Button CompoundsToReactions;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckedListBox ElementBox;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.CheckedListBox CompoundBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
    }
}

