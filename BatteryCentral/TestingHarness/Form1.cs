﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Pluming;
using MongoDB.Bson;
using System.Numerics;

namespace TestingHarness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ElementalCRUD CRUD = new ElementalCRUD("Chaos");
        private void button1_Click(object sender, EventArgs e)
        {
            List<string> elements = new List<string>();
            
            foreach(var v in ElementBox.CheckedItems)
            {
                elements.Add(v.ToString());
            }
            IEnumerable<Element> element_s = ElementHelper.StringsToElements(elements);

            CompactQuery query = CRUD.GetCompactQuery(element_s);

            CompoundBox.Items.Clear();

            KeyBox.Clear();
            foreach (CompactMolecule cm in CRUD.GetCompactMolecules(query))
            {
                CompoundBox.Items.Add(cm.formula);
                KeyBox.Add(cm.formula, cm);
            }

        }

        Dictionary<string, CompactMolecule> KeyBox = new Dictionary<string, CompactMolecule>();

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (Element element in ElementHelper.AllElements)
            {
                ElementBox.Items.Add(element.ToString());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<CompactMolecule> molecules = new List<CompactMolecule>();
            Dictionary<CompactMolecule, int> reactionCoeffecients = new Dictionary<CompactMolecule, int>();


            foreach (var v in CompoundBox.CheckedItems)
            {
                molecules.Add(KeyBox[v.ToString()]);
            }
            
            //List<string> LeadAcid = new List<string>();

            //LeadAcid.Add("H2SO4");
            //LeadAcid.Add("PbSO");
            //LeadAcid.Add("PbO2");
            //LeadAcid.Add("H2O");
            //LeadAcid.Add("PbO");

            //SpaceMap sm = eqv.IsValidEquation(molecules, out reactionCoeffecients);


            //switch (sm)
            //{
            //    case SpaceMap.UniqueSpace:
            //        richTextBox2.Text = "Balanced Unique Equation Found: \r\n" + GenerateStringFromReaction(reactionCoeffecients);
            //        break;
            //    case SpaceMap.PrimordialSpace:
            //        richTextBox2.Text = "Primorial Space, add another compound.";
            //        break;
            //    case SpaceMap.LinearSpace:
            //        richTextBox2.Text ="Linear Combination Space, remove a compound.";
            //        break;
            //}

           
        }

        private string GenerateStringFromReaction(Dictionary<CompactMolecule,int> reaction)
        {
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<CompactMolecule,int> cm in reaction)
            {
                if (cm.Value > 0)
                {
                    sb.Append(cm.Value + "*" + cm.Key.formula + "\t");
                }
            }
            sb.Append(" = ");
            foreach (KeyValuePair<CompactMolecule, int> cm in reaction)
            {
                if (cm.Value < 0)
                {
                    sb.Append(Math.Abs(cm.Value) + "*" + cm.Key.formula + "\t");
                }
            }
            
            return sb.ToString();
        }

        private void CompoundsToReactions_Click(object sender, EventArgs e)
        {
        }

        private void CompoundBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompactMolecule cm = KeyBox[CompoundBox.SelectedItem.ToString()];

            StringBuilder sb = new StringBuilder();
            foreach (MSGPMolecule mol in CRUD.GetMolecules(cm.MSGPMolecules))
            {
                sb.AppendLine("Formula:                 " + mol.PrettyFormula);
                sb.AppendLine("BandGap:                 " + mol.BandGap);
                sb.AppendLine("Density:                 " +mol.Density);
                sb.AppendLine("EAboveHull:              " + mol.EAboveHull);
                sb.AppendLine("Energy:                  " + mol.Energy);
                sb.AppendLine("EnergyPerAtom:           " + mol.EnergyPerAtom);
                sb.AppendLine("FormationEnergyPerAtom:  " + mol.FormationEnergyPerAtom);
                sb.AppendLine("OxideType:               " + mol.OxideType);
                sb.AppendLine("TotalMagnetization:      " + mol.TotalMagnetization);
                sb.AppendLine("Volume:                  " + mol.Volume);
                sb.AppendLine("\r\n");
            }
            richTextBox2.Text = sb.ToString();
        }

    }

}
