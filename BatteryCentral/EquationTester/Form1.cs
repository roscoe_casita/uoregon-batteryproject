﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Numerics;
using Pluming;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using System.IO;

namespace EquationTester
{
    public partial class Form1 : Form
    {
        public delegate void PrintScreen(string screen);
        public delegate void AddReaction();
        public event PrintScreen SecondaryScreen;
        public event PrintScreen ReactionDumpScreen;
        public event AddReaction ReactionAddition;
        public Form1()
        {
            InitializeComponent();

            List<CompactMolecule> temp = CRUD.GetCompactMolecules().ToList();

            foreach (CompactMolecule cm in temp)
            {
                if (!NamesToMolecules.ContainsKey(cm.formula))
                {
                    NamesToMolecules.Add(cm.formula, cm);
                }
            }
            
            SecondaryScreen += new PrintScreen(Form1_SecondaryScreen);
            ReactionDumpScreen += new PrintScreen(Form1_ReactionDumps);
            ReactionAddition += new AddReaction(Form1_ReactionAddition);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            List<string> items = new List<string>();
            foreach (Element element in ElementHelper.AllElements)
            {
                items.Add(element.ToString());
            }
            ElementsCheckedBox.Items.AddRange(items.ToArray());
            this.WindowState = FormWindowState.Maximized;
        }
        List<CompactMolecule> SelectedCompounds
        {
            get
            {
                List<CompactMolecule> returnValue = new List<CompactMolecule>();
                foreach (object o in CompoundsCheckedBox.CheckedItems)
                {
                    string s = o.ToString();
                    CompactMolecule cm = NamesToMolecules[s];
                    returnValue.Add(cm);
                }
                return returnValue;
            }
        }

        List<Reaction> ReactionList = new List<Reaction>();
        Dictionary<string, CompactMolecule> NamesToMolecules = new Dictionary<string, CompactMolecule>();
        
        ElementalCRUD CRUD = new ElementalCRUD("Chaos");
        List<CompactMolecule> WorkingSet = null;


        private void ComputeCompoundReactions_Click(object sender, EventArgs e)
        {
            ReactionList.Clear();
            ReactionCheckBox.Items.Clear();
            WorkingSet = SelectedCompounds;

            thread= new System.Threading.Thread(ComputeWorkingSet);
            thread.Start();
        }
        System.Threading.Thread thread = null;
        static BigInteger universe = BigInteger.Pow(2, 100);

        private string GenerateInformation(List<CompactMolecule> molecules)
        {

            BigInteger N = new BigInteger(molecules.Count);
            BigInteger R = new BigInteger(Filter.Count + 1);
            BigInteger NmR = N - R;

            BigInteger NF = 1;
            BigInteger NmRF = 1;
            BigInteger RF = 1;

            StringBuilder print = new StringBuilder();

            print.AppendLine("Selected N compounds: " + N.ToString());

            while (N > 0)
            {
                NF = NF * N;
                N--;
            }
            while (R > 0)
            {
                RF = RF * R;
                R--;
            }

            while (NmR > 0)
            {
                NmRF = NmRF * NmR;
                NmR--;
            }

            BigInteger denom = NmRF * RF;

            Rational gustimate = new Rational(NF, denom);

            print.AppendLine("2^100 (Universe): \r\n" + universe);
            // print.AppendLine("N! : \r\n" + NF.ToString());
            //print.AppendLine("R!*(N-R)!: \r\n" + RF.ToString() + "*(" + NmRF.ToString() + ")");
            print.AppendLine("Maximum Unique Guess on Reactions: \r\n" + gustimate.ToString());
            return print.ToString();

        }

        private void ComputeWorkingSet()
        {
            try
            {
                StopReactions = false;

                StringBuilder print = new StringBuilder();

                print.Append(GenerateInformation(WorkingSet));

                ReactionDumpScreen(print.ToString());

                

                DateTime past_time = DateTime.Now;
                EnumerateReactionSpace ers = new EnumerateReactionSpace(WorkingSet);

                ReactionList.Clear();
                foreach (Reaction r in ers.EnumerateAllReactions())
                {
                    if (StopReactions)
                    {
                        break;
                    }
                    ReactionList.Add(r);

                }
                DateTime current_time = DateTime.Now;

                TimeSpan used_time = current_time - past_time;



                long reactions_per_tick = ers.TotalReactionCount / (used_time.Seconds + 1);


                print.AppendLine();
                print.AppendLine("Reactions / Per Second: " + reactions_per_tick);

                print.AppendLine("Total Reactions: " + ers.TotalReactionCount);
                print.AppendLine("Primorial Reactions: " + ers.PrimodialCount);
                print.AppendLine("Unique Reactions: " + ers.UniqueReactions);
                print.AppendLine("Linear Reactions: " + ers.LinearCount);
                print.AppendLine();
                print.AppendLine("Total Enumerations: " + ers.Enumerations);
                print.AppendLine("Total Dive Ops: " + ers.DiveOps);
                print.AppendLine("Total Advance Ops: " + ers.AdvanceOps);
                print.AppendLine("Total Unwind Ops: " + ers.UnwindOps);
                print.AppendLine();

                for (int i = 0; i < MAX_REACT_COUNT & i < ReactionList.Count; i++)
                {
                    Reaction r = ReactionList[i];
                
                    print.AppendLine(r.DisplayString(CRUD));
                }
                 
                ReactionDumpScreen(print.ToString());
                ReactionAddition();
                
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                while (ex != null)
                {
                    sb.AppendLine(ex.Message);
                    ex = ex.InnerException;
                }

                ReactionDumpScreen(sb.ToString());
                sb.Clear();
            }
        }

        private const int MAX_REACT_COUNT = 50;
        void Form1_ReactionAddition()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(ReactionAddition);
            }
            else
            {
                for (int i = 0; i < ReactionList.Count & i < MAX_REACT_COUNT; i++)
                {
                    Reaction r = ReactionList[i];
                    ReactionCheckBox.Items.Add(r.DisplayString(CRUD));
                }
            }
        }

        void Form1_ReactionDumps(string screen)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(ReactionDumpScreen, screen);
            }
            else
            {
                ReactionDumps.Text = screen;
            }
        }

        void Form1_SecondaryScreen(string screen)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(SecondaryScreen, screen);
            }
            else
            {
                SecondaryInformation.Text = screen;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> items = new List<string>();

            foreach (var v in ElementsCheckedBox.CheckedItems)
            {
                items.Add(v.ToString());
            }
            ChangeCheckedElements(items);
        }

        List<Element> Filter = new List<Element>();
        private void ChangeCheckedElements(List<string> elements)
        {
            Filter.Clear();
            foreach (string s in elements)
            {
                Filter.Add(ElementHelper.StringToElement(s));
            }
            CompoundsCheckedBox.Items.Clear();
            List<CompactMolecule> AddItems = new List<CompactMolecule>();
            foreach (CompactMolecule cm in NamesToMolecules.Values)
            {
                bool add = true;
                foreach (Element e in cm.Elements)
                {
                    if (!Filter.Contains(e))
                    {
                        add = false;
                        break;
                    }
                }
                if (add)
                {
                    AddItems.Add(cm);
                }
            }

            AddItems.Sort((X, Y) => ElementHelper.CompareCompounds(X, Y));

            foreach (CompactMolecule cm in AddItems)
            {
                CompoundsCheckedBox.Items.Add(cm.formula);
            }

        }

        private void SelectAllCompounds_Click(object sender, EventArgs e)
        {
            bool allSelected = true;

            for (int i = 0; i < CompoundsCheckedBox.Items.Count; i++)
            {
                if (!CompoundsCheckedBox.GetItemChecked(i))
                {
                    allSelected = false;
                    break;
                }
            }


            for (int i = 0; i < CompoundsCheckedBox.Items.Count; i++)
            {
                CompoundsCheckedBox.SetItemChecked(i, !allSelected);
            }

            if (CompoundsCheckedBox.Items.Count > 0)
            {
                CompoundsCheckedBox.SelectedIndex = 0;
            }
        }

        private void CompoundsCheckedBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CompoundsCheckedBox.SelectedItem != null)
            {
                string name = CompoundsCheckedBox.SelectedItem.ToString();

                CompactMolecule cm = NamesToMolecules[name];

                StringBuilder sb = new StringBuilder();

                int count = 0;
                foreach (MSGPMolecule mol in cm.GetMolecules(CRUD))
                {
                    count++;
                    sb.AppendLine("-----------------------------------------");
                    sb.AppendLine(mol.ToString());
                    sb.AppendLine();
                    sb.AppendLine("-----------------------------------------");
                }
                string first = "Total Number of Entries for Molecule: " + count + "\r\n\r\n";
                sb.Insert(0, first);

                SecondaryScreen(sb.ToString());
                sb.Clear();

                ReactionDumpScreen(GenerateInformation(SelectedCompounds));
            }
            else
            {
                SecondaryScreen("");
            }
        }

        private void ReactionSet_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LoadReactionSet_Click(object sender, EventArgs e)
        {
            if (ReactionSet.SelectedItem == null)
            {
                return;
            }
            List<string> elements = new List<string>();
            List<string> compoundNames = new List<string>();
            switch (ReactionSet.SelectedItem.ToString())
            {
                case "Water":
                    elements.Add("H");
                    elements.Add("O");
                    compoundNames.Add("H2");
                    compoundNames.Add("O2");
                    compoundNames.Add("H4O2");
                    break;
                case "Lead Acid":
                    elements.Add("Pb");
                    elements.Add("O");
                    elements.Add("S");
                    elements.Add("H");
                    compoundNames.Add("Pb");
                    compoundNames.Add("PbO2");
                    compoundNames.Add("H2SO4");
                    compoundNames.Add("PbSO4");
                    compoundNames.Add("H2O");
                    break;
                case "Nicad":
                    elements.Add("O");
                    elements.Add("H");
                    elements.Add("Ni");
                    elements.Add("Cd");
                    compoundNames.Add("NiO(HO)");
                    compoundNames.Add("Cd");
                    compoundNames.Add("H2O");
                    compoundNames.Add("Ni(HO)2");
                    compoundNames.Add("Cd(HO)2");
                    break;
                case "Li+ Ion":
                    break;
            }
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Loading Reaction: " + ReactionSet.SelectedItem.ToString() );

            sb.AppendLine("Selecting Elements:");
            foreach (string element in elements)
            {
                sb.AppendFormat("{0,10}\r\n", element);
            }
            ChangeCheckedElements(elements);

            for (int i = 0; i < ElementsCheckedBox.Items.Count; i++)
            {
                string item_name = ElementsCheckedBox.Items[i].ToString();

                if (elements.Contains(item_name))
                {
                    ElementsCheckedBox.SetItemChecked(i, true);
                    elements.Remove(item_name);
                }
                else
                {
                    ElementsCheckedBox.SetItemChecked(i, false);
                }
            }

            if (elements.Count > 0)
            {
                sb.AppendLine("Elements not selected / found:");
                foreach (string element in elements)
                {
                    sb.AppendFormat("{0,10}\r\n", element);
                }
            }
            else
            {
                sb.AppendLine("All Reaction Elements Selected.");
            }

            sb.AppendLine("Selecting Compounds:");
            foreach (string compound in compoundNames)
            {
                sb.AppendFormat("{0,30}\r\n", compound);
            }

            for (int i = 0; i < CompoundsCheckedBox.Items.Count; i++)
            {
                string item_name = CompoundsCheckedBox.Items[i].ToString();

                if (compoundNames.Contains(item_name))
                {
                    CompoundsCheckedBox.SetItemChecked(i, true);
                    CompoundsCheckedBox.SetSelected(i, true);
                    compoundNames.Remove(item_name);
                }
                else
                {
                    CompoundsCheckedBox.SetItemChecked(i, false);
                }
            }

            if (compoundNames.Count > 0)
            {
                sb.AppendLine("\r\nERROR: Failed to select/find Compounds: \r\n");
                foreach (string compound in compoundNames)
                {
                    sb.AppendFormat("{0,30}\r\n", compound);
                }
            }
            else
            {
                sb.AppendLine("All Compounds Selected Successfully.\r\n");
            }


            ReactionDumpScreen(sb.ToString());
        }

        private void FindRelatedReactions_Click(object sender, EventArgs e)
        {
            ReactionCheckBox.Items.Clear();

            foreach (Reaction r in ReactionList)
            {
                foreach (CompactMolecule cm in SelectedCompounds)
                {
                    if (r.ContainsMolecule(cm))
                    {
                        ReactionCheckBox.Items.Add(r.DisplayString(CRUD));
                        break;
                    }
                }
            }
        }

        private void FindIntersectedReactions_Click(object sender, EventArgs e)
        {
            ReactionCheckBox.Items.Clear();

            foreach(Reaction r in ReactionList)
            {
                bool check = true;

                foreach (CompactMolecule cm in SelectedCompounds)
                {
                    if (!r.ContainsMolecule(cm))
                    {
                        check = false;
                        break;
                    }
                }
                if(check)
                {
                    
                    ReactionCheckBox.Items.Add(r.DisplayString(CRUD));
                    if (ReactionCheckBox.Items.Count >= MAX_REACT_COUNT)
                    {
                        break;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StopReactions = true;
        }
        bool StopReactions = false;

        private void ReactionCheckBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ReactionCheckBox.SelectedItem == null)
            {
                return;
            }
            List<Reaction> activeReactions = new List<Reaction>();

            foreach (var v in ReactionCheckBox.CheckedItems)
            {
                foreach (Reaction r in ReactionList)
                {
                    if (r.DisplayString(CRUD).CompareTo(v.ToString()) == 0)
                    {
                        activeReactions.Add(r);
                        break;
                    }
                }
            }

            if (activeReactions.Count <= 0)
            {
                return;
            }

            string graphviz_display = EnumerateReactionSpace.GenerateGraphVizReactions(activeReactions,CRUD);

            Image i = GetImage(graphviz_display);

            NetworkImage.Image = i;
            NetworkImage.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        // get the image as a byte array.
        public Image GetImage(string graph)
        {
            graph = graph.Replace("H4O2", "H2O").Replace("H8O8", "H2O2");
            string d = Directory.GetCurrentDirectory();
            string ImageFile = d + "\\GraphViz\\temp.png";
            string DotFile = d + "\\GraphViz\\temp.dot";


            if (File.Exists(ImageFile))
            {
                File.Delete(ImageFile);
            }

            if (File.Exists(DotFile))
            {
                File.Delete(DotFile);
            }
            File.WriteAllText(DotFile, graph);


            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C dot -Teps temp.dot -o temp.eps";
            startInfo.WorkingDirectory = d + "\\GraphViz\\";
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            if (!File.Exists(ImageFile))
                return null;

            Byte[] bytes = File.ReadAllBytes(ImageFile);
            Image i = Image.FromStream(new MemoryStream(bytes));
            return i;
        }



        private void SelectAllReactions_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ReactionCheckBox.Items.Count ; i++)
            {
                ReactionCheckBox.SetItemChecked(i, true);
            }
            if (ReactionCheckBox.Items.Count > 0)
            {
                ReactionCheckBox.SelectedIndex = 0;
            }
        }

    }
}
