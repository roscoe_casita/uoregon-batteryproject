﻿namespace EquationTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ElementsCheckedBox = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.LoadReactionSet = new System.Windows.Forms.Button();
            this.ReactionSet = new System.Windows.Forms.ComboBox();
            this.SelectAllCompounds = new System.Windows.Forms.Button();
            this.ComputeCompoundReactions = new System.Windows.Forms.Button();
            this.FindIntersectedReactions = new System.Windows.Forms.Button();
            this.FindRelatedReactions = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SelectAllReactions = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.CompoundsCheckedBox = new System.Windows.Forms.CheckedListBox();
            this.SecondaryInformation = new System.Windows.Forms.RichTextBox();
            this.ReactionDumps = new System.Windows.Forms.RichTextBox();
            this.NetworkImage = new System.Windows.Forms.PictureBox();
            this.ReactionCheckBox = new System.Windows.Forms.CheckedListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NetworkImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.SuspendLayout();
            // 
            // ElementsCheckedBox
            // 
            this.ElementsCheckedBox.CheckOnClick = true;
            this.ElementsCheckedBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ElementsCheckedBox.FormattingEnabled = true;
            this.ElementsCheckedBox.Location = new System.Drawing.Point(0, 0);
            this.ElementsCheckedBox.Margin = new System.Windows.Forms.Padding(2);
            this.ElementsCheckedBox.Name = "ElementsCheckedBox";
            this.ElementsCheckedBox.Size = new System.Drawing.Size(113, 383);
            this.ElementsCheckedBox.TabIndex = 6;
            this.ElementsCheckedBox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.LoadReactionSet, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ReactionSet, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.SelectAllCompounds, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.ComputeCompoundReactions, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.FindIntersectedReactions, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.FindRelatedReactions, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.button1, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.SelectAllReactions, 0, 7);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(244, 383);
            this.tableLayoutPanel2.TabIndex = 9;
            // 
            // LoadReactionSet
            // 
            this.LoadReactionSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadReactionSet.Location = new System.Drawing.Point(2, 51);
            this.LoadReactionSet.Margin = new System.Windows.Forms.Padding(2);
            this.LoadReactionSet.Name = "LoadReactionSet";
            this.LoadReactionSet.Size = new System.Drawing.Size(240, 45);
            this.LoadReactionSet.TabIndex = 0;
            this.LoadReactionSet.Text = "Load Reaction Set";
            this.LoadReactionSet.UseVisualStyleBackColor = true;
            this.LoadReactionSet.Click += new System.EventHandler(this.LoadReactionSet_Click);
            // 
            // ReactionSet
            // 
            this.ReactionSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactionSet.FormattingEnabled = true;
            this.ReactionSet.Items.AddRange(new object[] {
            "Water",
            "Lead Acid",
            "Nicad",
            "Li+ Ion"});
            this.ReactionSet.Location = new System.Drawing.Point(2, 2);
            this.ReactionSet.Margin = new System.Windows.Forms.Padding(2);
            this.ReactionSet.Name = "ReactionSet";
            this.ReactionSet.Size = new System.Drawing.Size(240, 21);
            this.ReactionSet.TabIndex = 1;
            this.ReactionSet.SelectedIndexChanged += new System.EventHandler(this.ReactionSet_SelectedIndexChanged);
            // 
            // SelectAllCompounds
            // 
            this.SelectAllCompounds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectAllCompounds.Location = new System.Drawing.Point(2, 100);
            this.SelectAllCompounds.Margin = new System.Windows.Forms.Padding(2);
            this.SelectAllCompounds.Name = "SelectAllCompounds";
            this.SelectAllCompounds.Size = new System.Drawing.Size(240, 45);
            this.SelectAllCompounds.TabIndex = 2;
            this.SelectAllCompounds.Text = "Select All Compounds";
            this.SelectAllCompounds.UseVisualStyleBackColor = true;
            this.SelectAllCompounds.Click += new System.EventHandler(this.SelectAllCompounds_Click);
            // 
            // ComputeCompoundReactions
            // 
            this.ComputeCompoundReactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComputeCompoundReactions.Location = new System.Drawing.Point(2, 149);
            this.ComputeCompoundReactions.Margin = new System.Windows.Forms.Padding(2);
            this.ComputeCompoundReactions.Name = "ComputeCompoundReactions";
            this.ComputeCompoundReactions.Size = new System.Drawing.Size(240, 45);
            this.ComputeCompoundReactions.TabIndex = 3;
            this.ComputeCompoundReactions.Text = "Compute Compound Reactions";
            this.ComputeCompoundReactions.UseVisualStyleBackColor = true;
            this.ComputeCompoundReactions.Click += new System.EventHandler(this.ComputeCompoundReactions_Click);
            // 
            // FindIntersectedReactions
            // 
            this.FindIntersectedReactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FindIntersectedReactions.Location = new System.Drawing.Point(2, 247);
            this.FindIntersectedReactions.Margin = new System.Windows.Forms.Padding(2);
            this.FindIntersectedReactions.Name = "FindIntersectedReactions";
            this.FindIntersectedReactions.Size = new System.Drawing.Size(240, 45);
            this.FindIntersectedReactions.TabIndex = 5;
            this.FindIntersectedReactions.Text = "Find Intersected Reactions";
            this.FindIntersectedReactions.UseVisualStyleBackColor = true;
            this.FindIntersectedReactions.Click += new System.EventHandler(this.FindIntersectedReactions_Click);
            // 
            // FindRelatedReactions
            // 
            this.FindRelatedReactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FindRelatedReactions.Location = new System.Drawing.Point(2, 296);
            this.FindRelatedReactions.Margin = new System.Windows.Forms.Padding(2);
            this.FindRelatedReactions.Name = "FindRelatedReactions";
            this.FindRelatedReactions.Size = new System.Drawing.Size(240, 45);
            this.FindRelatedReactions.TabIndex = 4;
            this.FindRelatedReactions.Text = "Find Related Reactions";
            this.FindRelatedReactions.UseVisualStyleBackColor = true;
            this.FindRelatedReactions.Click += new System.EventHandler(this.FindRelatedReactions_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(2, 198);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 45);
            this.button1.TabIndex = 6;
            this.button1.Text = "Stop Current Reactions";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SelectAllReactions
            // 
            this.SelectAllReactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectAllReactions.Location = new System.Drawing.Point(3, 346);
            this.SelectAllReactions.Name = "SelectAllReactions";
            this.SelectAllReactions.Size = new System.Drawing.Size(238, 34);
            this.SelectAllReactions.TabIndex = 7;
            this.SelectAllReactions.Text = "Select All Reactions";
            this.SelectAllReactions.UseVisualStyleBackColor = true;
            this.SelectAllReactions.Click += new System.EventHandler(this.SelectAllReactions_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ElementsCheckedBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.CompoundsCheckedBox);
            this.splitContainer1.Size = new System.Drawing.Size(383, 383);
            this.splitContainer1.SplitterDistance = 113;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 10;
            // 
            // CompoundsCheckedBox
            // 
            this.CompoundsCheckedBox.CheckOnClick = true;
            this.CompoundsCheckedBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompoundsCheckedBox.FormattingEnabled = true;
            this.CompoundsCheckedBox.Location = new System.Drawing.Point(0, 0);
            this.CompoundsCheckedBox.Margin = new System.Windows.Forms.Padding(2);
            this.CompoundsCheckedBox.Name = "CompoundsCheckedBox";
            this.CompoundsCheckedBox.Size = new System.Drawing.Size(267, 383);
            this.CompoundsCheckedBox.TabIndex = 8;
            this.CompoundsCheckedBox.SelectedIndexChanged += new System.EventHandler(this.CompoundsCheckedBox_SelectedIndexChanged);
            // 
            // SecondaryInformation
            // 
            this.SecondaryInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SecondaryInformation.Font = new System.Drawing.Font("Consolas", 8F);
            this.SecondaryInformation.Location = new System.Drawing.Point(0, 0);
            this.SecondaryInformation.Margin = new System.Windows.Forms.Padding(2);
            this.SecondaryInformation.Name = "SecondaryInformation";
            this.SecondaryInformation.Size = new System.Drawing.Size(474, 383);
            this.SecondaryInformation.TabIndex = 11;
            this.SecondaryInformation.Text = "";
            // 
            // ReactionDumps
            // 
            this.ReactionDumps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactionDumps.Location = new System.Drawing.Point(0, 0);
            this.ReactionDumps.Margin = new System.Windows.Forms.Padding(2);
            this.ReactionDumps.Name = "ReactionDumps";
            this.ReactionDumps.Size = new System.Drawing.Size(242, 313);
            this.ReactionDumps.TabIndex = 12;
            this.ReactionDumps.Text = "";
            this.ReactionDumps.WordWrap = false;
            // 
            // NetworkImage
            // 
            this.NetworkImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NetworkImage.Location = new System.Drawing.Point(0, 0);
            this.NetworkImage.Margin = new System.Windows.Forms.Padding(2);
            this.NetworkImage.Name = "NetworkImage";
            this.NetworkImage.Size = new System.Drawing.Size(476, 313);
            this.NetworkImage.TabIndex = 13;
            this.NetworkImage.TabStop = false;
            // 
            // ReactionCheckBox
            // 
            this.ReactionCheckBox.CheckOnClick = true;
            this.ReactionCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactionCheckBox.FormattingEnabled = true;
            this.ReactionCheckBox.Location = new System.Drawing.Point(0, 0);
            this.ReactionCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ReactionCheckBox.Name = "ReactionCheckBox";
            this.ReactionCheckBox.Size = new System.Drawing.Size(383, 313);
            this.ReactionCheckBox.TabIndex = 14;
            this.ReactionCheckBox.SelectedIndexChanged += new System.EventHandler(this.ReactionCheckBox_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer2.Size = new System.Drawing.Size(1109, 700);
            this.splitContainer2.SplitterDistance = 383;
            this.splitContainer2.TabIndex = 8;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(1109, 383);
            this.splitContainer3.SplitterDistance = 244;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.SecondaryInformation);
            this.splitContainer4.Size = new System.Drawing.Size(861, 383);
            this.splitContainer4.SplitterDistance = 383;
            this.splitContainer4.TabIndex = 0;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.ReactionDumps);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(1109, 313);
            this.splitContainer5.SplitterDistance = 242;
            this.splitContainer5.TabIndex = 9;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.ReactionCheckBox);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.NetworkImage);
            this.splitContainer6.Size = new System.Drawing.Size(863, 313);
            this.splitContainer6.SplitterDistance = 383;
            this.splitContainer6.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 700);
            this.Controls.Add(this.splitContainer2);
            this.Name = "Form1";
            this.Text = "Ve\'ger 1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NetworkImage)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox ElementsCheckedBox;
        private System.Windows.Forms.CheckedListBox CompoundsCheckedBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button LoadReactionSet;
        private System.Windows.Forms.ComboBox ReactionSet;
        private System.Windows.Forms.Button SelectAllCompounds;
        private System.Windows.Forms.Button ComputeCompoundReactions;
        private System.Windows.Forms.Button FindRelatedReactions;
        private System.Windows.Forms.Button FindIntersectedReactions;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox SecondaryInformation;
        private System.Windows.Forms.RichTextBox ReactionDumps;
        private System.Windows.Forms.PictureBox NetworkImage;
        private System.Windows.Forms.CheckedListBox ReactionCheckBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Button SelectAllReactions;
    }
}

