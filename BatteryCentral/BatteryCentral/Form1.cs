﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.ServiceModel;
using System.Runtime.Serialization.Json;
using System.Collections;
using System.Reflection;
using Codeplex.Data;
using System.Threading;
using System.Numerics;

using Pluming;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;

namespace BatteryCentral
{
    public partial class Form1 : Form
    {
        delegate void PostString(string s);
        event PostString Post;

        public Form1()
        {
            InitializeComponent();
            Post += new PostString(Form1_Post);

        }
        StreamWriter sw;

        void Form1_Post(string s)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(Post, s);
            }
            else
            {
                lock (sb)
                {
                    sb.AppendLine(s);
                    OutputConsole.Text = sb.ToString();
                    sw.WriteLine(s);

                    if (sb.Length > 1000)
                    {
                        sw.Flush();
                        sb = sb.Clear();
                    }
                }
            }
        }

        StringBuilder sb = new StringBuilder();




        void downloader_Post(string s)
        {
            Post(s);
        }
        int Split { get { return Environment.ProcessorCount * 2; } }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!File.Exists("DatabaseIDs.ids"))
            {
                MessageBox.Show("Need DatabaseIDs.ids file from pytamagen.");
                return;
            }

            List<TaskHandler> handlers = TaskMaster.WebJSONHelers(Split);
            TaskMaster.Dispatch(handlers, new TaskMaster.PostMessage(downloader_Post));
            DisableAll();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            sw = new StreamWriter("Output.txt", true);
        }
        private void DisableAll()
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<TaskHandler> handlers = TaskMaster.DatabaseCompactors(Split);
            TaskMaster.Dispatch(handlers, new TaskMaster.PostMessage(downloader_Post));
            DisableAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<TaskHandler> handlers = TaskMaster.QueryBuilder(Split);
            TaskMaster.Dispatch(handlers, new TaskMaster.PostMessage(downloader_Post));
            DisableAll();
        }

    }

}